<?php

/**
 * @file
 * Contains zabbixtemplate.page.inc.
 *
 * Page callback for Zabbixtemplate entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Zabbixtemplate templates.
 *
 * Default template: zabbixtemplate.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zabbixtemplate(array &$variables) {
  // Fetch zabbixtemplate Entity Object.
  $zabbixtemplate = $variables['elements']['#zabbixtemplate'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
