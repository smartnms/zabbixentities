<?php

/**
 * @file
 * Contains zabbixgroup.page.inc.
 *
 * Page callback for Group as defined in Zabbix entities.
 */

use Drupal\Core\Render\Element;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Prepares variables for Group as defined in Zabbix templates.
 *
 * Default template: zabbixgroup.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zabbixgroup(array &$variables) {
  // Fetch zabbixgroup Entity Object.
  $zabbixgroup = $variables['elements']['#zabbixgroup'];
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
