<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Group as defined in Zabbix entities.
 *
 * @ingroup zabbixentities
 */
class zabbixgroupListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Local Group ID');
    $header['group_id'] = $this->t('Zabbix Group ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\zabbixentities\Entity\zabbixgroup */
    $row['id'] = $entity->id();
    $row['group_id'] = $entity->getGroupid();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.zabbixgroup.canonical', [
          'zabbixgroup' => $entity->id(),
        ]
      )
    );
    return $row + parent::buildRow($entity);
  }

    public function render() {
        $build['#attached']['library'][] = 'zabbixentities/smartnms';
        return $build + parent::render();
    }
}
