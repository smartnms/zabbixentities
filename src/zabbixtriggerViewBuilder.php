<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Form;
use Drupal\Core\Url;
use Drupal\zabbixentities\Entity\zabbixhost;

// TODO revisar visualización del trigger, probar con inline, dar un poco de estilo
// TODO zabbix url debe ser un parámetro  de configuración.
/**
 * Render controller for zabbixhost.
 */

class zabbixtriggerViewBuilder extends EntityViewBuilder {


    public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {

        if (empty($entities)) {
            return;
        }

        parent::buildComponents($build, $entities, $displays, $view_mode);

        $build['#attached']['library'][] = 'zabbixentities/smartnms';
        foreach ($entities as $id => $entity) {
            // We put the display components on a different detail
            $build[$id]['fields']=array('#type'=>'details',
                '#title' => $this->t('Trigger Details'),
                '#description' => t('Custom values for trigger'),
                '#open' => FALSE,);
            foreach($build[$id] as $name => $attribute) {
                if ($entity->hasField($name)) {
                    $build[$id]['fields'][$name] = $attribute;
                    unset($build[$id][$name]);
                    }
                }

            $build[$id]['chart']=array('#type'=>'details',
                                    '#title' => $this->t('Charts'),
                                    '#description' => t('Trigger Chart Values'),
                                    '#open' => TRUE,);
            $stime=time()-3600; //1h ago
            $api=new zabbix_api();
            try {
                $result=$api->itemGet(array('output'=>array('name','itemid'),'triggerids'=>$entity->getTriggerid()));
            } catch (Exception $e){
                    drupal_set_message(t('Error loading from '.$api->getApiUrl()));
                    break;
            }
            $itemids=array();
            foreach($result as $delta) {
                $itemids[] = $delta['itemid'];
                $name = $delta['name'];
            }
            $count=count($itemids);
            if($count>0) {
                $build[$id]['chart']['title']= $name;
                    $result = $api->graphGet(array('output' => 'graphid',
                        'itemids' => $itemids));
                    foreach($result as $chart) {
                        $build[$id]['chart']['graphs'][] = [
                            '#theme' => 'image',
                            '#uri' => 'http://82.223.78.156/zabbix/chart2.php?graphid=' . $chart['graphid'] . '&stime=' . $stime,
                            '#width' => '100%'];

                    }
                }
            $result=$api->templateGet(array('output'=>'templateid','triggerids'=>$entity->getTriggerid()));
            $templateids=array();
            foreach($result as $delta)
                $templateids[]=$delta['templateid'];
            $count=count($templateids);
            if($count>0) {
                $result = $api->graphGet(array('output' => 'graphid',
                    'templateids' => $templateids));
                foreach($result as $chart)
                    $build[$id]['chart']['graphs'][] = [
                        '#theme' => 'image',
                        '#uri' => 'http://82.223.78.156/zabbix/chart2.php?graphid='.$chart['graphid'].'&stime='.$stime,
                        '#width' => '100%'];
            }
            if(!array_key_exists('graphs',$build[$id]['chart'])){
                $build[$id]['chart']['graphs'] = array('#markup'=>t('There are no graphs linked to this trigger'));
                }
        }
    }
}

?>