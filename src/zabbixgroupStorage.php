<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixgroupInterface;

/**
 * Defines the storage handler class for Group as defined in Zabbix entities.
 *
 * This extends the base storage class, adding required special handling for
 * Group as defined in Zabbix entities.
 *
 * @ingroup zabbixentities
 */
class zabbixgroupStorage extends SqlContentEntityStorage implements zabbixgroupStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(zabbixgroupInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {zabbixgroup_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {zabbixgroup_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(zabbixgroupInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {zabbixgroup_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('zabbixgroup_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
