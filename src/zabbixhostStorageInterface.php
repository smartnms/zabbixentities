<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixhostInterface;

/**
 * Defines the storage handler class for Zabbixhost entities.
 *
 * This extends the base storage class, adding required special handling for
 * Zabbixhost entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixhostStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Zabbixhost revision IDs for a specific Zabbixhost.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixhostInterface $entity
   *   The Zabbixhost entity.
   *
   * @return int[]
   *   Zabbixhost revision IDs (in ascending order).
   */
  public function revisionIds(zabbixhostInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Zabbixhost author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Zabbixhost revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixhostInterface $entity
   *   The Zabbixhost entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(zabbixhostInterface $entity);

  /**
   * Unsets the language for all Zabbixhost with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
