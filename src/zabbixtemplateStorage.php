<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixtemplateInterface;

/**
 * Defines the storage handler class for Zabbixtemplate entities.
 *
 * This extends the base storage class, adding required special handling for
 * Zabbixtemplate entities.
 *
 * @ingroup zabbixentities
 */
class zabbixtemplateStorage extends SqlContentEntityStorage implements zabbixtemplateStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(zabbixtemplateInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {zabbixtemplate_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {zabbixtemplate_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(zabbixtemplateInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {zabbixtemplate_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('zabbixtemplate_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
