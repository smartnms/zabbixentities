<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;
use Drupal\zabbixfrontend\zabbix_api;

/**
 * Defines a class to build a listing of Zabbixtrigger entities.
 *
 * @ingroup zabbixentities
 */
class zabbixtriggerListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['trigger_id'] =$this->t('Zabbix ID');
    $header['name'] = $this->t('Name');
    $header['expression'] = $this->t('Expression');
    $header['status'] = $this->t('Status');
    $header['priority'] = $this->t('Priority');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\zabbixentities\Entity\zabbixtrigger */

    $priorities=array($this->t('not classified'),
                        $this->t('information'),
                        $this->t('warning'),
                        $this->t('average'),
                        $this->t('high'),
                        $this->t('disaster'),);
    $row['trigger_id'] = $entity->getTriggerid();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.zabbixtrigger.canonical', [
          'zabbixtrigger' => $entity->id(),
        ]
      )
    );
    /*
     * Trigger expression: {<server>:<key>.<function>(<parameter>)}<operator><constant>
     */
    $row['expression']='{'.$entity->getServer().':'.$entity->getKey().'.'.$entity->getFunction().'('.$entity->getParameter().')}'.$entity->getOperator().$entity->getConstant();
    $api=new zabbix_api();
    $result=$api->triggerGet(array('filter'=>array('triggerid'=>$entity->getTriggerid()),'output'=>array('state','status','value','priority')));
    if($result[0]['status']==1)
        $status=$this->t('Disabled');
    else if($result[0]['state']==1)
        $status=$this->t('Unknown');
    else if($result[0]['value']==1)
        $status=$this->t('Problem');
    else
        $status=$this->t('OK');
    $row['status']=$status;
    $row['priority']['data']=array('#type'=>'html_tag','#tag'=>'span','#value'=>$priorities[$result[0]['priority']]);
    $row['priority']['data']['#attributes']=array('class' => 'trigger_'.$result[0]['priority']);
    $row['priority']['data']['#allowed_tags']=array('div','span');
    return $row + parent::buildRow($entity);
  }

    public function render() {
        $build['#attached']['library'][] = 'zabbixentities/smartnms';
        return $build + parent::render();
    }

}
