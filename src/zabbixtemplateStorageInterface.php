<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixtemplateInterface;

/**
 * Defines the storage handler class for Zabbixtemplate entities.
 *
 * This extends the base storage class, adding required special handling for
 * Zabbixtemplate entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixtemplateStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Zabbixtemplate revision IDs for a specific Zabbixtemplate.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixtemplateInterface $entity
   *   The Zabbixtemplate entity.
   *
   * @return int[]
   *   Zabbixtemplate revision IDs (in ascending order).
   */
  public function revisionIds(zabbixtemplateInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Zabbixtemplate author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Zabbixtemplate revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixtemplateInterface $entity
   *   The Zabbixtemplate entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(zabbixtemplateInterface $entity);

  /**
   * Unsets the language for all Zabbixtemplate with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
