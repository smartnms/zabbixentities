<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixtriggerInterface;

/**
 * Defines the storage handler class for Zabbixtrigger entities.
 *
 * This extends the base storage class, adding required special handling for
 * Zabbixtrigger entities.
 *
 * @ingroup zabbixentities
 */
class zabbixtriggerStorage extends SqlContentEntityStorage implements zabbixtriggerStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(zabbixtriggerInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {zabbixtrigger_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {zabbixtrigger_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(zabbixtriggerInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {zabbixtrigger_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('zabbixtrigger_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
