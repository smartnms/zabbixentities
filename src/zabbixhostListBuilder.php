<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Defines a class to build a listing of Zabbixhost entities.
 *
 * @ingroup zabbixentities
 */
class zabbixhostListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  public $usefilter=FALSE;
  public $entity_ids;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Host ID');
    $header['host_id'] = $this->t('Zabbix ID');
    $header['name'] = $this->t('Name');
    $header['dashboard']=$this->t('Dashboard');
    $header['status']=$this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity)
  {
      /* @var $entity \Drupal\zabbixentities\Entity\zabbixhost */
      $row['id'] = $entity->id();
      $row['host_id'] = $entity->getHostid();
      $row['name'] = $this->l(
          $entity->label(),
          new Url(
              'entity.zabbixhost.canonical', [
                  'zabbixhost' => $entity->id(),
              ]
          )
      );
      //TODO Cambiar l que está deprecated
      //TODO Crear un valor por defecto para el dashboard
      if ($entity->getDashboard() == '')
          $row['dashboard'] = 'No Definido';
      else
          $row['dashboard'] = $this->l('Dashboard', Url::fromUri($entity->getDashboard(), array('attributes' => ['target' => '_blank'])));
      $row['status'] = array('data'=>$entity->renderStatus());
      return $row + parent::buildRow($entity);
      }

    public function render() {
        $build['#attached']['library'][] = 'zabbixentities/smartnms';
        return $build + parent::render();
    }

    public function load() {
        if(!$this->usefilter)
            $this->entity_ids = $this->getEntityIds();
        return $this->storage->loadMultiple($this->entity_ids);
    }
}
