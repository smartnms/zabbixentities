<?php

namespace Drupal\zabbixentities\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'zabbixinterface_field_type' field type.
 *
 * @FieldType(
 *   id = "zabbixinterface_field_type",
 *   label = @Translation("Zabbixinterface field type"),
 *   description = @Translation("Zabbix Host Interface"),
 *   default_widget = "zabbixinterface_widget_type",
 *   default_formatter = "zabbixinterface_formatter_type"
 * )
 */
class zabbixinterfaceFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 255,
      'is_ascii' => FALSE,
      'case_sensitive' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
      $properties['interfaceid'] = DataDefinition::create('integer')
          ->setLabel(new TranslatableMarkup('Interface ID in Zabbix.'));
      $properties['dns'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('DNS name used by the interface.'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'));
      $properties['ip']= DataDefinition::create('string')
          ->setLabel(new TranslatableMarkup('IP address used by the interface.'))
          ->setRequired(TRUE);
      $properties['main']= DataDefinition::create('integer')
          ->setLabel(new TranslatableMarkup('Whether the interface is used as default on the host. Only one interface of some type can be set as default on a host.'))
          ->setSetting('allowed_values',['0' =>t('Not default'),'1' =>t('Default'),])
          ->setSetting('default','0')
          ->setRequired(TRUE);
      $properties['type']= DataDefinition::create('integer')
          ->setLabel(new TranslatableMarkup('Interface type'))
          ->setSetting('allowed_values',['1' =>t('agent'),'2' =>'SNMP','3' =>'IPMI','4' =>'JMX',])
          ->setSetting('default','1')
          ->setRequired(TRUE);
      $properties['port']= DataDefinition::create('integer')
          ->setLabel(new TranslatableMarkup('Interface port'))
          ->setSetting('default','10050')
          ->setRequired(TRUE);
      $properties['useip']= DataDefinition::create('integer')
          ->setLabel(new TranslatableMarkup('Whether the connection should be made via IP. '))
          ->setSetting('allowed_values',['0' =>t('connect using host DNS name'),'1' =>t('connect using host IP address for this host interface'),])
          ->setSetting('default','1')
          ->setRequired(TRUE);
      return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
          'interfaceid'=>[
              'type' => 'int',
              'not null' => TRUE,
              'default'=>'0',
          ],
        'dns' => [
          'type' => $field_definition->getSetting('is_ascii') === TRUE ? 'varchar_ascii' : 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
          'binary' => $field_definition->getSetting('case_sensitive'),
        ],
          'ip'=>[
              'type' => $field_definition->getSetting('is_ascii') === TRUE ? 'varchar_ascii' : 'varchar',
              'length' => (int) $field_definition->getSetting('max_length'),
              'binary' => $field_definition->getSetting('case_sensitive'),],
          'main'=>[
              'type' => 'int',
              'not null' => TRUE,
              'default'=>'0',
              ],
          'port'=>[
              'type' => 'int',
              'not null' => TRUE,
              'default'=>'0',
          ],
          'type'=>[
              'type' => 'int',
              'not null' => TRUE,
              'default'=>'0',
          ],
          'useip'=>[
              'type' => 'int',
              'not null' => TRUE,
              'allowed_values' =>
              [
              '0' => t('connect using host DNS name'),
              '1' => t('connect using host IP address for this host interface'),
              ],
          'default'=>'1',
          ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    if ($max_length = $this->getSetting('max_length')) {
      $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
      $constraints[] = $constraint_manager->create('ComplexData', [
          'interfaceid' => [],
        'dns' => [
          'Length' => [
            'max' => $max_length,
            'maxMessage' => t('%name: may not be longer than @max characters.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max' => $max_length
            ]),
          ],
        ],
          'ip' =>[],
          'main'=>[],
          'port'=>[],
          'type'=>[],
          'useip'=>[],
      ]);
    }

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['dns'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
      $values['ip'] = "127.0.0.1";
      $values['main'] = 0;
      $values['port'] = 666;
      $values['type'] = 0;
      $values['useip'] = 1;


      return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['max_length'] = [
      '#type' => 'number',
      '#title' => t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => t('The maximum length of the field in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('ip')->getValue();
    return $value === NULL || $value === '';
  }

}
