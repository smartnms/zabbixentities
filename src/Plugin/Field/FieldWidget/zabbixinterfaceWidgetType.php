<?php

namespace Drupal\zabbixentities\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'zabbixinterface_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "zabbixinterface_widget_type",
 *   label = @Translation("Zabbixinterface widget type"),
 *   field_types = {
 *     "zabbixinterface_field_type"
 *   }
 * )
 */
class zabbixinterfaceWidgetType extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['wrapper']=array('#type'=>'details',
          '#title' => $this->t('Interface Details'),
          '#description' => t('Details for the main interface of the host'),
          '#open' => TRUE,);
    $element['wrapper']['dns'] = $element + [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#title' => $this->t('DNS Name of the Interface'),
    ];
      $element['wrapper']['ip']= [
          '#type' => 'textfield',
          '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
          '#size' => 15,
          '#placeholder' => $this->getSetting('placeholder'),
          '#maxlength' => 15,
          '#title' => $this->t('IP Address'),
      ];
      $element['wrapper']['main']= [
          '#type' => 'select',
          '#placeholder' => $this->getSetting('placeholder'),
          '#options' =>['0' =>t('Not default'),'1' =>t('Default'),],
          '#title' => $this->t('Is it the main interface?'),
      ];
      $element['wrapper']['type']= [
          '#type' => 'select',
          '#placeholder' => $this->getSetting('placeholder'),
          '#options' =>['1' =>t('agent'),'2' =>'SNMP','3' =>'IPMI','4' =>'JMX',],
          '#title' => $this->t('Interface Type'),
      ];
      $element['wrapper']['port']= [
          '#type' => 'textfield',
          '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
          '#size' => 5,
          '#placeholder' => $this->getSetting('placeholder'),
          '#maxlength' => 5,
          '#title' => $this->t('Port number'),
      ];
      $element['wrapper']['useip']= [
              '#type' => 'select',
              '#placeholder' => $this->getSetting('placeholder'),
              '#title' => $this->t('Use IP or DNS'),
              '#options' =>['0' =>t('connect using host DNS name'),'1' =>t('connect using host IP address for this host interface'),],
          ];

    return $element;
  }

}
