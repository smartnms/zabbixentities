<?php

namespace Drupal\zabbixentities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\zabbixentities\Entity\zabbixtriggerInterface;

/**
 * Class zabbixtriggerController.
 *
 *  Returns responses for Zabbixtrigger routes.
 *
 * @package Drupal\zabbixentities\Controller
 */
class zabbixtriggerController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Zabbixtrigger  revision.
   *
   * @param int $zabbixtrigger_revision
   *   The Zabbixtrigger  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($zabbixtrigger_revision) {
    $zabbixtrigger = $this->entityManager()->getStorage('zabbixtrigger')->loadRevision($zabbixtrigger_revision);
    $view_builder = $this->entityManager()->getViewBuilder('zabbixtrigger');

    return $view_builder->view($zabbixtrigger);
  }

  /**
   * Page title callback for a Zabbixtrigger  revision.
   *
   * @param int $zabbixtrigger_revision
   *   The Zabbixtrigger  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($zabbixtrigger_revision) {
    $zabbixtrigger = $this->entityManager()->getStorage('zabbixtrigger')->loadRevision($zabbixtrigger_revision);
    return $this->t('Revision of %title from %date', ['%title' => $zabbixtrigger->label(), '%date' => format_date($zabbixtrigger->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Zabbixtrigger .
   *
   * @param \Drupal\zabbixentities\Entity\zabbixtriggerInterface $zabbixtrigger
   *   A Zabbixtrigger  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(zabbixtriggerInterface $zabbixtrigger) {
    $account = $this->currentUser();
    $langcode = $zabbixtrigger->language()->getId();
    $langname = $zabbixtrigger->language()->getName();
    $languages = $zabbixtrigger->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $zabbixtrigger_storage = $this->entityManager()->getStorage('zabbixtrigger');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $zabbixtrigger->label()]) : $this->t('Revisions for %title', ['%title' => $zabbixtrigger->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all zabbixtrigger revisions") || $account->hasPermission('administer zabbixtrigger entities')));
    $delete_permission = (($account->hasPermission("delete all zabbixtrigger revisions") || $account->hasPermission('administer zabbixtrigger entities')));

    $rows = [];

    $vids = $zabbixtrigger_storage->revisionIds($zabbixtrigger);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\zabbixentities\zabbixtriggerInterface $revision */
      $revision = $zabbixtrigger_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $zabbixtrigger->getRevisionId()) {
          $link = $this->l($date, new Url('entity.zabbixtrigger.revision', ['zabbixtrigger' => $zabbixtrigger->id(), 'zabbixtrigger_revision' => $vid]));
        }
        else {
          $link = $zabbixtrigger->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.zabbixtrigger.translation_revert', ['zabbixtrigger' => $zabbixtrigger->id(), 'zabbixtrigger_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.zabbixtrigger.revision_revert', ['zabbixtrigger' => $zabbixtrigger->id(), 'zabbixtrigger_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.zabbixtrigger.revision_delete', ['zabbixtrigger' => $zabbixtrigger->id(), 'zabbixtrigger_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['zabbixtrigger_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
