<?php

namespace Drupal\zabbixentities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\zabbixentities\Entity\zabbixtemplateInterface;

/**
 * Class zabbixtemplateController.
 *
 *  Returns responses for Zabbixtemplate routes.
 *
 * @package Drupal\zabbixentities\Controller
 */
class zabbixtemplateController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Zabbixtemplate  revision.
   *
   * @param int $zabbixtemplate_revision
   *   The Zabbixtemplate  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($zabbixtemplate_revision) {
    $zabbixtemplate = $this->entityManager()->getStorage('zabbixtemplate')->loadRevision($zabbixtemplate_revision);
    $view_builder = $this->entityManager()->getViewBuilder('zabbixtemplate');

    return $view_builder->view($zabbixtemplate);
  }

  /**
   * Page title callback for a Zabbixtemplate  revision.
   *
   * @param int $zabbixtemplate_revision
   *   The Zabbixtemplate  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($zabbixtemplate_revision) {
    $zabbixtemplate = $this->entityManager()->getStorage('zabbixtemplate')->loadRevision($zabbixtemplate_revision);
    return $this->t('Revision of %title from %date', ['%title' => $zabbixtemplate->label(), '%date' => format_date($zabbixtemplate->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Zabbixtemplate .
   *
   * @param \Drupal\zabbixentities\Entity\zabbixtemplateInterface $zabbixtemplate
   *   A Zabbixtemplate  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(zabbixtemplateInterface $zabbixtemplate) {
    $account = $this->currentUser();
    $langcode = $zabbixtemplate->language()->getId();
    $langname = $zabbixtemplate->language()->getName();
    $languages = $zabbixtemplate->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $zabbixtemplate_storage = $this->entityManager()->getStorage('zabbixtemplate');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $zabbixtemplate->label()]) : $this->t('Revisions for %title', ['%title' => $zabbixtemplate->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all zabbixtemplate revisions") || $account->hasPermission('administer zabbixtemplate entities')));
    $delete_permission = (($account->hasPermission("delete all zabbixtemplate revisions") || $account->hasPermission('administer zabbixtemplate entities')));

    $rows = [];

    $vids = $zabbixtemplate_storage->revisionIds($zabbixtemplate);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\zabbixentities\zabbixtemplateInterface $revision */
      $revision = $zabbixtemplate_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $zabbixtemplate->getRevisionId()) {
          $link = $this->l($date, new Url('entity.zabbixtemplate.revision', ['zabbixtemplate' => $zabbixtemplate->id(), 'zabbixtemplate_revision' => $vid]));
        }
        else {
          $link = $zabbixtemplate->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.zabbixtemplate.translation_revert', ['zabbixtemplate' => $zabbixtemplate->id(), 'zabbixtemplate_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.zabbixtemplate.revision_revert', ['zabbixtemplate' => $zabbixtemplate->id(), 'zabbixtemplate_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.zabbixtemplate.revision_delete', ['zabbixtemplate' => $zabbixtemplate->id(), 'zabbixtemplate_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['zabbixtemplate_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
