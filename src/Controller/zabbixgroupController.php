<?php

namespace Drupal\zabbixentities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\zabbixentities\Entity\zabbixgroupInterface;

/**
 * Class zabbixgroupController.
 *
 *  Returns responses for Group as defined in Zabbix routes.
 *
 * @package Drupal\zabbixentities\Controller
 */
class zabbixgroupController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Group as defined in Zabbix  revision.
   *
   * @param int $zabbixgroup_revision
   *   The Group as defined in Zabbix  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($zabbixgroup_revision) {
    $zabbixgroup = $this->entityManager()->getStorage('zabbixgroup')->loadRevision($zabbixgroup_revision);
    $view_builder = $this->entityManager()->getViewBuilder('zabbixgroup');

    return $view_builder->view($zabbixgroup);
  }

  /**
   * Page title callback for a Group as defined in Zabbix  revision.
   *
   * @param int $zabbixgroup_revision
   *   The Group as defined in Zabbix  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($zabbixgroup_revision) {
    $zabbixgroup = $this->entityManager()->getStorage('zabbixgroup')->loadRevision($zabbixgroup_revision);
    return $this->t('Revision of %title from %date', ['%title' => $zabbixgroup->label(), '%date' => format_date($zabbixgroup->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Group as defined in Zabbix .
   *
   * @param \Drupal\zabbixentities\Entity\zabbixgroupInterface $zabbixgroup
   *   A Group as defined in Zabbix  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(zabbixgroupInterface $zabbixgroup) {
    $account = $this->currentUser();
    $langcode = $zabbixgroup->language()->getId();
    $langname = $zabbixgroup->language()->getName();
    $languages = $zabbixgroup->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $zabbixgroup_storage = $this->entityManager()->getStorage('zabbixgroup');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $zabbixgroup->label()]) : $this->t('Revisions for %title', ['%title' => $zabbixgroup->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all group as defined in zabbix revisions") || $account->hasPermission('administer group as defined in zabbix entities')));
    $delete_permission = (($account->hasPermission("delete all group as defined in zabbix revisions") || $account->hasPermission('administer group as defined in zabbix entities')));

    $rows = [];

    $vids = $zabbixgroup_storage->revisionIds($zabbixgroup);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\zabbixentities\zabbixgroupInterface $revision */
      $revision = $zabbixgroup_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $zabbixgroup->getRevisionId()) {
          $link = $this->l($date, new Url('entity.zabbixgroup.revision', ['zabbixgroup' => $zabbixgroup->id(), 'zabbixgroup_revision' => $vid]));
        }
        else {
          $link = $zabbixgroup->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.zabbixgroup.translation_revert', ['zabbixgroup' => $zabbixgroup->id(), 'zabbixgroup_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.zabbixgroup.revision_revert', ['zabbixgroup' => $zabbixgroup->id(), 'zabbixgroup_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.zabbixgroup.revision_delete', ['zabbixgroup' => $zabbixgroup->id(), 'zabbixgroup_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['zabbixgroup_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
