<?php

namespace Drupal\zabbixentities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\zabbixentities\Entity\zabbixhostInterface;

/**
 * Class zabbixhostController.
 *
 *  Returns responses for Zabbixhost routes.
 *
 * @package Drupal\zabbixentities\Controller
 */
class zabbixhostController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Zabbixhost  revision.
   *
   * @param int $zabbixhost_revision
   *   The Zabbixhost  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($zabbixhost_revision) {
    $zabbixhost = $this->entityManager()->getStorage('zabbixhost')->loadRevision($zabbixhost_revision);
    $view_builder = $this->entityManager()->getViewBuilder('zabbixhost');

    return $view_builder->view($zabbixhost);
  }

  /**
   * Page title callback for a Zabbixhost  revision.
   *
   * @param int $zabbixhost_revision
   *   The Zabbixhost  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($zabbixhost_revision) {
    $zabbixhost = $this->entityManager()->getStorage('zabbixhost')->loadRevision($zabbixhost_revision);
    return $this->t('Revision of %title from %date', ['%title' => $zabbixhost->label(), '%date' => format_date($zabbixhost->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Zabbixhost .
   *
   * @param \Drupal\zabbixentities\Entity\zabbixhostInterface $zabbixhost
   *   A Zabbixhost  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(zabbixhostInterface $zabbixhost) {
    $account = $this->currentUser();
    $langcode = $zabbixhost->language()->getId();
    $langname = $zabbixhost->language()->getName();
    $languages = $zabbixhost->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $zabbixhost_storage = $this->entityManager()->getStorage('zabbixhost');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $zabbixhost->label()]) : $this->t('Revisions for %title', ['%title' => $zabbixhost->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all zabbixhost revisions") || $account->hasPermission('administer zabbixhost entities')));
    $delete_permission = (($account->hasPermission("delete all zabbixhost revisions") || $account->hasPermission('administer zabbixhost entities')));

    $rows = [];

    $vids = $zabbixhost_storage->revisionIds($zabbixhost);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\zabbixentities\zabbixhostInterface $revision */
      $revision = $zabbixhost_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $zabbixhost->getRevisionId()) {
          $link = $this->l($date, new Url('entity.zabbixhost.revision', ['zabbixhost' => $zabbixhost->id(), 'zabbixhost_revision' => $vid]));
        }
        else {
          $link = $zabbixhost->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.zabbixhost.translation_revert', ['zabbixhost' => $zabbixhost->id(), 'zabbixhost_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.zabbixhost.revision_revert', ['zabbixhost' => $zabbixhost->id(), 'zabbixhost_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.zabbixhost.revision_delete', ['zabbixhost' => $zabbixhost->id(), 'zabbixhost_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['zabbixhost_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
