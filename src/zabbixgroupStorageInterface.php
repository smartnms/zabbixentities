<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixgroupInterface;

/**
 * Defines the storage handler class for Group as defined in Zabbix entities.
 *
 * This extends the base storage class, adding required special handling for
 * Group as defined in Zabbix entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixgroupStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Group as defined in Zabbix revision IDs for a specific Group as defined in Zabbix.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixgroupInterface $entity
   *   The Group as defined in Zabbix entity.
   *
   * @return int[]
   *   Group as defined in Zabbix revision IDs (in ascending order).
   */
  public function revisionIds(zabbixgroupInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Group as defined in Zabbix author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Group as defined in Zabbix revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixgroupInterface $entity
   *   The Group as defined in Zabbix entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(zabbixgroupInterface $entity);

  /**
   * Unsets the language for all Group as defined in Zabbix with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
