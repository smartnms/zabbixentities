<?php

namespace Drupal\zabbixentities;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for zabbixtemplate.
 */
class zabbixtemplateTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
