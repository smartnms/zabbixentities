<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Group as defined in Zabbix entity.
 *
 * @see \Drupal\zabbixentities\Entity\zabbixgroup.
 */
class zabbixgroupAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\zabbixentities\Entity\zabbixgroupInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished group as defined in zabbix entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published group as defined in zabbix entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit group as defined in zabbix entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete group as defined in zabbix entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add group as defined in zabbix entities');
  }

}
