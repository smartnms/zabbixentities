<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixhostInterface;

/**
 * Defines the storage handler class for Zabbixhost entities.
 *
 * This extends the base storage class, adding required special handling for
 * Zabbixhost entities.
 *
 * @ingroup zabbixentities
 */
class zabbixhostStorage extends SqlContentEntityStorage implements zabbixhostStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(zabbixhostInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {zabbixhost_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {zabbixhost_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(zabbixhostInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {zabbixhost_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('zabbixhost_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
