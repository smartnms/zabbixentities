<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Zabbixtemplate entity.
 *
 * @see \Drupal\zabbixentities\Entity\zabbixtemplate.
 */
class zabbixtemplateAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\zabbixentities\Entity\zabbixtemplateInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished zabbixtemplate entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published zabbixtemplate entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit zabbixtemplate entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete zabbixtemplate entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add zabbixtemplate entities');
  }

}
