<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Zabbixhost entity.
 *
 * @see \Drupal\zabbixentities\Entity\zabbixhost.
 */
class zabbixhostAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\zabbixentities\Entity\zabbixhostInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished zabbixhost entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published zabbixhost entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit zabbixhost entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete zabbixhost entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add zabbixhost entities');
  }

}
