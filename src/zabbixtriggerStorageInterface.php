<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\zabbixentities\Entity\zabbixtriggerInterface;

/**
 * Defines the storage handler class for Zabbixtrigger entities.
 *
 * This extends the base storage class, adding required special handling for
 * Zabbixtrigger entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixtriggerStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Zabbixtrigger revision IDs for a specific Zabbixtrigger.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixtriggerInterface $entity
   *   The Zabbixtrigger entity.
   *
   * @return int[]
   *   Zabbixtrigger revision IDs (in ascending order).
   */
  public function revisionIds(zabbixtriggerInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Zabbixtrigger author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Zabbixtrigger revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\zabbixentities\Entity\zabbixtriggerInterface $entity
   *   The Zabbixtrigger entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(zabbixtriggerInterface $entity);

  /**
   * Unsets the language for all Zabbixtrigger with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
