<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class zabbixhostTypeForm.
 *
 * @package Drupal\zabbixentities\Form
 */
class zabbixhostTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $zabbixhost_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $zabbixhost_type->label(),
      '#description' => $this->t("Label for the Zabbixhost type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $zabbixhost_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\zabbixentities\Entity\zabbixhostType::load',
      ],
      '#disabled' => !$zabbixhost_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $zabbixhost_type = $this->entity;
    $status = $zabbixhost_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Zabbixhost type.', [
          '%label' => $zabbixhost_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Zabbixhost type.', [
          '%label' => $zabbixhost_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($zabbixhost_type->toUrl('collection'));
  }

}
