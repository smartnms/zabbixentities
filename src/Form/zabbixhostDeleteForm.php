<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Zabbixhost entities.
 *
 * @ingroup zabbixentities
 */
class zabbixhostDeleteForm extends ContentEntityDeleteForm {


}
