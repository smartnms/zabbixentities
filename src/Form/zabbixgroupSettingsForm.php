<?php

namespace Drupal\zabbixentities\Form;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class zabbixgroupSettingsForm.
 *
 * @package Drupal\zabbixentities\Form
 *
 * @ingroup zabbixentities
 */
class zabbixgroupSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'zabbixgroup_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      $api=new zabbix_api();
      $result=$api->hostgroupGet(array('output'=>'extend'));
      foreach($result as $row=>$group) {
          $data[$row] = array(
              'group_id' => $group['groupid'],
              'name' => $group['name']
          );
      }
      foreach($data as $group) {
          $entity = Drupal::entityManager()
              ->getStorage('zabbixgroup_default')
              ->create($group);
          $entity->save();
      }

  }

  /**
   * Defines the settings form for Group as defined in Zabbix entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['zabbixgroup_settings']['#markup'] = t('Use this fucntion to import all groups from a new Zabbix server, using twice will duplicate groups');
    $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Import Groups from Zabbix'),
          '#button_type' => 'primary',
      );
      return $form;
    return $form;
  }

}
