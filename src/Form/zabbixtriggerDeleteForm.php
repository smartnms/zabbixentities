<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Zabbixtrigger entities.
 *
 * @ingroup zabbixentities
 */
class zabbixtriggerDeleteForm extends ContentEntityDeleteForm {


}
