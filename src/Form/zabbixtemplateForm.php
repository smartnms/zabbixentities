<?php

namespace Drupal\zabbixentities\Form;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Zabbixtemplate edit forms.
 *
 * @ingroup zabbixentities
 */
class zabbixtemplateForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\zabbixentities\Entity\zabbixtemplate */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
 	  $form['template_id']['#disabled']='disabled';
    }
	else
	  $form['template_id']['#type']='hidden';

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }
	$api=new zabbix_api();
    $group_field=$entity->get('group_id');
    $groups=array();
    $hosts=array();
    if(!empty($group_field))
        {
            $group_entities=$group_field->referencedEntities();
            foreach($group_entities as $group_entity)
            {
                $groups[]=array('groupid',$group_entity->getGroupid());
            }
        }
    $host_field=$entity->get('host_id');
    if(!empty($host_field))
        {
            $host_entities = $host_field->referencedEntities();
            foreach ($host_entities as $host_entity)
            {
              $hosts[] = array('hostid', $host_entity->getHostid());
            }
        }
    if ($this->entity->isNew()) {
		$result=$api->templateCreate(array('host'=>$entity->getName(),
                                            'description'=>$entity->getDescription(),
                                            'groups'=>$groups,
                                            'hosts'=>$hosts));
		$entity->setTemplateid($result[0]['templateids'][0]);
	}
	else {
		$result=$api->templateUpdate(array('templateid'=>$entity->getTemplateid(),
                                            'host'=>$entity->getName(),
                                            'description'=>$entity->getDescription(),
                                            'hosts'=>$hosts));
	}
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Zabbixtemplate.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Zabbixtemplate.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.zabbixtemplate.canonical', ['zabbixtemplate' => $entity->id()]);
  }

}
