<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Group as defined in Zabbix entities.
 *
 * @ingroup zabbixentities
 */
class zabbixgroupDeleteForm extends ContentEntityDeleteForm {


}
