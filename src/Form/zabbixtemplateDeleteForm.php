<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Zabbixtemplate entities.
 *
 * @ingroup zabbixentities
 */
class zabbixtemplateDeleteForm extends ContentEntityDeleteForm {


}
