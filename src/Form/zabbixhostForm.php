<?php

namespace Drupal\zabbixentities\Form;
use Drupal\zabbixfrontend\Exception;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form;
use Drupal\Core\Logger;
use Drupal\glpiinventory\glpi_api;

/**
 * Form controller for Zabbixhost edit forms.
 *
 * @ingroup zabbixentities
 */
class zabbixhostForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\zabbixentities\Entity\zabbixhost */

      /**
       * Names for GLPI fields
       */

  $glpi_tags=array('id' => 'ID:',
      'entities_id' => t('Entity ID:'),
      'name' => t('Name:'),
      'serial' => t('Serial Number:'),
      'otherserial' => t('Additional Serial Number:'),
      'status_id'=> t('Status'),'contact' => t('Contact:'),
      'contact_num' => t('Contact Number:'),
      'users_id_tech' => t('ID of technical user:'),
      'groups_id_tech' => t('ID of technical Group:'),
      'comment' => t('Comment'),
      'date_mod' => t('Last modified:'),
      'operatingsystems_id' => t('Operating System ID'),
      'operatingsystemversions_id' => t('Operating system Version ID:'),
      'operatingsystemservicepacks_id' => t('Service Pack ID:'),
      'operatingsystemarchitectures_id' => t('Architecture ID'),
      'os_license_number' => t('OS License Number:'),
      'os_licenseid' =>t('OS License ID:'),
      'os_kernel_version' => t('OS Kernel Version:'),
      'locations_id' => t('Locations ID:'),
      'domains_id' => t('Domains ID:'),
      'networks_id' => t('Networks ID:' ),
      'computermodels_id' => t('Computer Model ID:'),
      'computertypes_id' => t('Computer Type ID:'),
      'manufacturers_id' => t('Manufacturer ID:'),
      'date_creation' => t('Creation Date:'));

    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

      /**
       * If entity is not new we offer to create a new revision
       */
    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => -10,
      ];
    }

    /**
     * Entity Fields are added to a fieldset (Detail)
     */
      $form['fields']=array('#type'=>'details',
          '#title' => $this->t('Host Details'),
          '#description' => t('Custom values for host'),
          '#open' => TRUE,
          '#weight' => -9,);


      foreach($form as $name => $attribute) {
          if ($entity->hasField($name)&&$name!='interface') {
              $form['fields'][$name] = $attribute;
              unset($form[$name]);
          }
      }

      /**
       * TODO we load host details from Zabbix if they are empty
       */
      $interface=$entity->getInterface();
      /**
       * If there is an interface ID, interface is already defined in database so we load it into form
       */
      if (!$this->entity->isNew() && is_numeric($interface['interfaceid'])) {
          $form['interface']['#default_value']=$interface;
          $form['interface']['#weight'] = -8;
      }
      else{
      /**
       * We import first interface from Zabbix
       */
          try {
              $api = new zabbix_api();
              $result = $api->hostinterfaceGet(array('hostids' => $entity->getHostid(),
                  'output' => 'extend'));
          }
          catch(Exception $e){
              drupal_set_message('Error retrieving interface: '.$e->getMessage());
          }
        unset($result[0]['bulk']);
        unset($result[0]['hostid']);
        foreach($result['0'] as $label => $value)
          {
              $form['interface']['widget']['0']['wrapper'][$label]['#default_value']=$value;
          }
        $form['interface']['#weight'] = -8;
      }


      /**
       *  We load host details from GLPI
       */
    $form['glpi']=array('#type'=>'details',
          '#title' => $this->t('GLPI inventory Details'),
          '#description' => t('Inventory values for host'),
          '#open' => TRUE,);
    $form['os']=array('#type'=>'details',
          '#title' => $this->t('Operating System Details'),
          '#description' => t('Operating System values for host'),
          '#open' => TRUE,);
    $api=new glpi_api();
    $itemtype='Computer';
    $loadoutput=$api->getitem($itemtype,$entity->getGlpiid(),array('expand_dropdowns'=>'true'));
    foreach($glpi_tags as $tag => $label){
        switch($tag) {
            case 'entities_id':
                $glpientities=$api->getitem('getMyEntities','','');
                $options=array();
                foreach($glpientities['myentities'] as $glpientity){
                    $options[$glpientity['id']]=$glpientity['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'locations_id':
                $glpilocations=$api->getitem('location','','');
                $options=array();
                foreach($glpilocations as $glpilocation){
                    $options[$glpilocation['id']]=$glpilocation['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'computertypes_id':
                $glpicomputertypes=$api->getitem('computertype','','');
                $options=array();
                foreach($glpicomputertypes as $glpicomputertype){
                    $options[$glpicomputertype['id']]=$glpicomputertype['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'status_id':
                $glpistates=$api->getitem('state','','');
                $options=array();
                foreach($glpistates as $glpistate){
                    $options[$glpistate['id']]=$glpistate['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'users_id_tech' :
                $glpiusers=$api->getitem('user','','');
                $options=array();
                foreach($glpiusers as $glpiuser){
                    $options[$glpiuser['id']]=$glpiuser['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'networks_id':
                $glpinetworks=$api->getitem('network','','');
                $options=array();
                foreach($glpinetworks as $glpinetwork){
                    $options[$glpinetwork['id']]=$glpinetwork['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'groups_id_tech':
                $glpigroups=$api->getitem('group','','');
                $options=array();
                foreach($glpigroups as $glpigroup){
                    $options[$glpigroup['id']]=$glpigroup['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'domains_id':
                $glpidomains=$api->getitem('domain','','');
                $options=array();
                foreach($glpidomains as $glpidomain){
                    $options[$glpidomain['id']]=$glpidomain['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'manufacturers_id':
                $glpimanufacturers=$api->getitem('manufacturer','','');
                $options=array();
                foreach($glpimanufacturers as $glpimanufacturer){
                    $options[$glpimanufacturer['id']]=$glpimanufacturer['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'computermodels_id':
                $glpicomputermodels=$api->getitem('computermodel','','');
                $options=array();
                foreach($glpicomputermodels as $glpicomputermodel){
                    $options[$glpicomputermodel['id']]=$glpicomputermodel['name'];
                }
                $form['glpi'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'operatingsystems_id':
                $glpioperatingsystems=$api->getitem('operatingsystem','','');
                $options=array();
                foreach($glpioperatingsystems as $glpioperatingsystem){
                    $options[$glpioperatingsystem['id']]=$glpioperatingsystem['name'];
                }
                $form['os'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'operatingsystemarchitectures_id':
                $glpioperatingsystemarchitectures=$api->getitem('operatingsystemarchitecture','','');
                $options=array();
                foreach($glpioperatingsystemarchitectures as $glpioperatingsystemarchitecture){
                    $options[$glpioperatingsystemarchitecture['id']]=$glpioperatingsystemarchitecture['name'];
                }
                $form['os'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'operatingsystemversions_id':
                $glpioperatingsystemversions=$api->getitem('operatingsystemversion','','');
                $options=array();
                foreach($glpioperatingsystemversions as $glpioperatingsystemversion){
                    $options[$glpioperatingsystemversion['id']]=$glpioperatingsystemversion['name'];
                }
                $form['os'][$tag] = array('#type' => 'select',
                    '#title' => $label,
                    '#options' => $options,
                    '#default_value' => array_search($loadoutput[$tag],$options));
                break;
            case 'os_license_number':
            case 'os_licenseid':
            case 'os_kernel_version':
            $form['os'][$tag] = array('#type' => 'textfield',
                '#title' => $label,
                '#default_value' => $loadoutput[$tag]);
                break;
            case 'name':
                $form['glpi']['glpi_name'] = array('#type' => 'textfield',
                    '#title' => $label,
                    '#default_value' => $loadoutput[$tag]);
                break;
            default:
                $form['glpi'][$tag] = array('#type' => 'textfield',
                    '#title' => $label,
                    '#default_value' => $loadoutput[$tag]);
        }
    }


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }
    $groupids=array();
    $groups=$entity->get('group_id')->referencedEntities();
    foreach($groups as $item)
    {
        if ($item->entity) {
            $groupids[] = $item->entity->getGroupid();
        }
    }

      $api=new zabbix_api();
      if ($this->entity->isNew()) {
          $result=$api->hostCreate(array('host'=>$entity->getName(),
              'groups'=>$groupids,
              'interfaces'=>$entity->get('interface')->getValue()));
          $entity->setHostid($result['hostids'][0]);
      }
      else {
          $result=$api->hostUpdate(array('hostid'=>$entity->getHostid(),
              'groups'=>$groupids,
              'interfaces'=>$entity->interface->getValue()));
      }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Zabbixhost.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Zabbixhost.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.zabbixhost.canonical', ['zabbixhost' => $entity->id()]);
  }

}
