<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class zabbixtemplateTypeForm.
 *
 * @package Drupal\zabbixentities\Form
 */
class zabbixtemplateTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $zabbixtemplate_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $zabbixtemplate_type->label(),
      '#description' => $this->t("Label for the Zabbixtemplate type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $zabbixtemplate_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\zabbixentities\Entity\zabbixtemplateType::load',
      ],
      '#disabled' => !$zabbixtemplate_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $zabbixtemplate_type = $this->entity;
    $status = $zabbixtemplate_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Zabbixtemplate type.', [
          '%label' => $zabbixtemplate_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Zabbixtemplate type.', [
          '%label' => $zabbixtemplate_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($zabbixtemplate_type->toUrl('collection'));
  }

}
