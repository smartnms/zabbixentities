<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Zabbixtemplate revision.
 *
 * @ingroup zabbixentities
 */
class zabbixtemplateRevisionDeleteForm extends ConfirmFormBase {


  /**
   * The Zabbixtemplate revision.
   *
   * @var \Drupal\zabbixentities\Entity\zabbixtemplateInterface
   */
  protected $revision;

  /**
   * The Zabbixtemplate storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $zabbixtemplateStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new zabbixtemplateRevisionDeleteForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $entity_storage, Connection $connection) {
    $this->zabbixtemplateStorage = $entity_storage;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('zabbixtemplate'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zabbixtemplate_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the revision from %revision-date?', ['%revision-date' => format_date($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.zabbixtemplate.version_history', ['zabbixtemplate' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $zabbixtemplate_revision = NULL) {
    $this->revision = $this->zabbixtemplateStorage->loadRevision($zabbixtemplate_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->zabbixtemplateStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Zabbixtemplate: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    drupal_set_message(t('Revision from %revision-date of Zabbixtemplate %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.zabbixtemplate.canonical',
       ['zabbixtemplate' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {zabbixtemplate_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.zabbixtemplate.version_history',
         ['zabbixtemplate' => $this->revision->id()]
      );
    }
  }

}
