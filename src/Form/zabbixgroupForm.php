<?php

namespace Drupal\zabbixentities\Form;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Group as defined in Zabbix edit forms.
 *
 * @ingroup zabbixentities
 */
class zabbixgroupForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\zabbixentities\Entity\zabbixgroup */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;
    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
      $form['group_id']=array('#type' => 'textfield',
            '#title' => $this->t('Group ID'),
            '#default_value' => $entity->getGroupid(),
            '#disabled'=>'disabled'
            );
	  $api=new zabbix_api();
	  $result=$api->hostgroupGet(array('groupids'=>array($entity->getGroupid())));
	  $type='';
      if($result[0]['internal']==0)
          $type.=t('Internal group|');
      else
          $type.=t('Not Internal Group|');
      if($result[0]['flags']==4)
          $type.=t('Discovered Host group');
      else
          $type.=t('Plain Host Group');
      $form['group_type']['#default_value']=$type;
    }
	else
	  $form['group_id']['#type']='hidden';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }
	$api=new zabbix_api();
    if ($entity->isNew()) {
		$result=$api->hostgroupCreate(array('name'=>$entity->getName()));
		$groupid=$result['groupids'][0];
		if(is_null($result['groupids'][0]))
        {
            $form_state->setErrorByName('group_id', t('Zabbix API returned empty group ID, something went wrong'));
        }
        else
        {
            $form_state->set('group_id', $groupid);
            $entity->setGroupid($groupid);
        }
	}
	else {
		$result=$api->hostgroupUpdate(array('groupid'=>$entity->getGroupid(),'name'=>$entity->getName()));
        $groupid=$result['groupids'][0];
        if(is_null($result['groupids'][0]))
            {
            $form_state->setErrorByName('group_id', t('Zabbix API returned empty group ID, something went wrong'));
        }
        elseif ($groupid!=$entity->getGroupid()) {
            $form_state->setErrorByName('group_id', t('Zabbix API returned incorrect group ID, something went wrong'));
        }

	}
    $status = parent::save($form, $form_state);
    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Group as defined in Zabbix.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Group as defined in Zabbix.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.zabbixgroup.canonical', ['zabbixgroup' => $entity->id()]);
  }

}
