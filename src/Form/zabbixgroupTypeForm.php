<?php

namespace Drupal\zabbixentities\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class zabbixgroupTypeForm.
 *
 * @package Drupal\zabbixentities\Form
 */
class zabbixgroupTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $zabbixgroup_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $zabbixgroup_type->label(),
      '#description' => $this->t("Label for the Group as defined in Zabbix type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $zabbixgroup_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\zabbixentities\Entity\zabbixgroupType::load',
      ],
      '#disabled' => !$zabbixgroup_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $zabbixgroup_type = $this->entity;
    $status = $zabbixgroup_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Group as defined in Zabbix type.', [
          '%label' => $zabbixgroup_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Group as defined in Zabbix type.', [
          '%label' => $zabbixgroup_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($zabbixgroup_type->toUrl('collection'));
  }

}
