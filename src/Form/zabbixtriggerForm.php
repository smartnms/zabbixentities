<?php

namespace Drupal\zabbixentities\Form;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Zabbixtrigger edit forms.
 *
 * @ingroup zabbixentities
 */
class zabbixtriggerForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\zabbixentities\Entity\zabbixtrigger */
    $form = parent::buildForm($form, $form_state);
    $allowed_operators=array('-','not','*','/','<=','>=','<>','<','>','=');
    $allowed_functions=array('abschange','avg','band','change','count','date','dayofmonth','dayofweek','delta','diff','forecast','fuzzytime','iregexp',
        'last','logeventid','logseverity','logsource','max','min','nodata','now','percentile','prev','regexp','str','strlen','sum','time','timeleft');

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    $entity = $this->entity;
    $server=$entity->getServer();
    $key=$entity->getKey();
    $function=$entity->getFunction();
    $parameter=$entity->getParameter();
    $operator=$entity->getOperator();
    $constant=$entity->getConstant();

    $form['expression']=array(
        '#type' => 'fieldset',
        '#title' => $this->t('Expression to evaluate in Trigger'),
    );
      $query = \Drupal::service('entity.query')
          ->get('zabbixhost')
          ->condition('name', $server);
      $entity_ids = $query->execute();
      $host_entity=\Drupal::entityTypeManager()->getStorage('zabbixhost')->load($entity_ids[0]);
      $api=new zabbix_api();
      $result=$api->itemGet(array('output'=>['key_'],
          'host'=>$server,
         ));
      $keys=array();
        foreach($result as $delta) {
            $key=$delta['key_'];
            $keys[$key]=$key;
        }

      /*
       * Trigger expression: {<server>:<key>.<function>(<parameter>)}<operator><constant>
       */
      $form['expression']['info']=array(
          '#markup' =>$this->t('<pre>Trigger expression: {<server>:<key>.<function>(<parameter>)}<operator><constant></pre>')
      );
      $form['expression']['host']=array('#type' => 'entity_reference',
        '#target_type' => 'zabbixhost',
        '#default_value' => $host_entity, // The #default_value can be either an entity object or an array of entity objects.
    );
      $form['expression']['key']=array(
          '#type' => 'select',
          '#title' => t('Key'),
          '#options' => $keys,
          '#default_value' => $key,
      );
      $form['expression']['function']=array(
          '#type' => 'select',
          '#title' => t('Function'),
          '#options' => $allowed_functions,
          '#default_value' => $function,
      );
      $form['expression']['parameter']=array(
          '#type' => 'textfield',
          '#title' => t('Parameter'),
          '#default_value' => $parameter,
      );
      $form['expression']['operator']=array(
          '#type' => 'select',
          '#title' => t('Operator'),
          '#options' => $allowed_operators,
          '#default_value' => $operator,
      );
      $form['expression']['constant']=array(
          '#type' => 'textfield',
          '#title' => t('Constant'),
          '#default_value' => $constant,
      );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }
      $api=new zabbix_api();
      if ($this->entity->isNew()) {
          $result=$api->triggerCreate(array('description'=>$entity->getName(),
              'expression'=>$entity->getExpression(),
              'comments'=>$entity->getComments()));
          $entity->setTriggerid($result['triggerids'][0]);
      }
      else {
          $result=$api->triggerUpdate(array('triggerids'=>$entity->getTriggerid(),
              'description'=>$entity->getName(),
              'expression'=>$entity->getExpression(),
              'comments'=>$entity->getComments()));
      }
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Zabbixtrigger.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Zabbixtrigger.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.zabbixtrigger.canonical', ['zabbixtrigger' => $entity->id()]);
  }

}
