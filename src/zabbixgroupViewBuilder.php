<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\zabbixfrontend\zabbix_api;
use Drupal\Core\Url;
use Drupal\zabbixentities\Entity\zabbixhost;


/**
 * Render controller for zabbixhost.
 */

class zabbixgroupViewBuilder extends EntityViewBuilder {


    public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {

        if (empty($entities)) {
            return;
        }

        parent::buildComponents($build, $entities, $displays, $view_mode);

        $build['#attached']['library'][] = 'zabbixentities/smartnms';
        foreach ($entities as $id => $entity) {
            // We put the display components on a different detail
            $build[$id]['fields']=array('#type'=>'details',
                '#title' => $this->t('Group Details'),
                '#description' => t('Custom values for group'),
                '#open' => TRUE,);
            foreach($build[$id] as $name => $attribute) {
                if ($entity->hasField($name)) {
                    $build[$id]['fields'][$name] = $attribute;
                    unset($build[$id][$name]);
                }
            }
            $zabbix_uri=\Drupal::l(t('Zabbix Link'), Url::fromUri("http://82.223.78.156/zabbix/latest.php?fullscreen=0&groupid=".$entity->getGroupid(),
                array('attributes' => ['target' => '_blank'])));

            $build[$id]['fields']['zabbix']= array('#type'=>'html_tag','#tag'=>'span','#value'=>$zabbix_uri);
            $build[$id]['fields']['zabbix']['#attributes']=array('class'=>['btn','btn-primary','btn-xs']);
            // Add Status to render array.
            $build[$id]['status']=array('#type'=>'details',
                                    '#title' => $this->t('Status'),
                                    '#description' => t('Status summary of hosts in the group'),
                                    '#open' => TRUE,);
            $api=new zabbix_api();
            $result=$api->hostGet(array('output'=>'hostid',
                'groupids'=>array($entity->getGroupid())));
            $hostids=array();
            foreach($result as $delta)
                $hostids[]=$delta['hostid'];
            if(count($hostids)>0) {
                $query = \Drupal::entityQuery('zabbixhost');
                $query->condition('host_id', $hostids, 'IN');
                $entity_ids = $query->execute();

                $listbuilder = \Drupal::entityTypeManager()->getListBuilder('zabbixhost');
                $listbuilder->entity_ids = $entity_ids;
                $listbuilder->usefilter = TRUE;
                $build[$id]['status']['componentes'] = $listbuilder->render();
            }
            else{
                $build[$id]['status']['componentes'] = array('#markup'=>t('There are no servers on this group'));
            }
        }

    }

}

?>