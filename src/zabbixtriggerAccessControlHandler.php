<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Zabbixtrigger entity.
 *
 * @see \Drupal\zabbixentities\Entity\zabbixtrigger.
 */
class zabbixtriggerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\zabbixentities\Entity\zabbixtriggerInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished zabbixtrigger entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published zabbixtrigger entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit zabbixtrigger entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete zabbixtrigger entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add zabbixtrigger entities');
  }

}
