<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Group as defined in Zabbix entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixgroupInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Group as defined in Zabbix name.
   *
   * @return string
   *   Name of the Group as defined in Zabbix.
   */
  public function getName();

  /**
   * Sets the Group as defined in Zabbix name.
   *
   * @param string $name
   *   The Group as defined in Zabbix name.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixgroupInterface
   *   The called Group as defined in Zabbix entity.
   */
  public function setName($name);

  /**
   * Gets the Group as defined in Zabbix creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Group as defined in Zabbix.
   */
  public function getCreatedTime();

  /**
   * Sets the Group as defined in Zabbix creation timestamp.
   *
   * @param int $timestamp
   *   The Group as defined in Zabbix creation timestamp.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixgroupInterface
   *   The called Group as defined in Zabbix entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Group as defined in Zabbix published status indicator.
   *
   * Unpublished Group as defined in Zabbix are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Group as defined in Zabbix is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Group as defined in Zabbix.
   *
   * @param bool $published
   *   TRUE to set this Group as defined in Zabbix to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixgroupInterface
   *   The called Group as defined in Zabbix entity.
   */
  public function setPublished($published);

  /**
   * Gets the Group as defined in Zabbix revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Group as defined in Zabbix revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixgroupInterface
   *   The called Group as defined in Zabbix entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Group as defined in Zabbix revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Group as defined in Zabbix revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixgroupInterface
   *   The called Group as defined in Zabbix entity.
   */
  public function setRevisionUserId($uid);

    /**
     * Gets the Group ID as defined in Zabbix.
     *
     * @return int
     *   The group id.
     */
    public function getGroupid();

    /**
     * Sets the Group ID in Zabbixx.
     *
     * @param int $group_id
     *   The group ID of the group in Zabbix.
     *
     * @return \Drupal\zabbixentities\Entity\zabbixgroupInterface
     *   The called Group as defined in Zabbix entity.
     */
    public function setGroupid($group_id);

    /**
     * Gets the Group Type as defined in Zabbix.
     *
     * @return string
     *   The group type.
     */
    public function getGrouptype();

    /**
     * Sets the Group Type in Zabbixx.
     *
     * @param string $group_type
     *   The group ID of the group in Zabbix.
     *
     * @return \Drupal\zabbixentities\Entity\zabbixgroupInterface
     *   The called Group as defined in Zabbix entity.
     */
    public function setGrouptype($group_type);
}
