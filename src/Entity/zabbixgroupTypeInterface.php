<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Group as defined in Zabbix type entities.
 */
interface zabbixgroupTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
