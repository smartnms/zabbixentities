<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Zabbixtrigger type entities.
 */
interface zabbixtriggerTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
