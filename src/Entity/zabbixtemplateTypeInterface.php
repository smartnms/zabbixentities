<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Zabbixtemplate type entities.
 */
interface zabbixtemplateTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
