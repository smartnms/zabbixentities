<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Zabbixhost entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixhostInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Zabbixhost name.
   *
   * @return string
   *   Name of the Zabbixhost.
   */
  public function getName();

  /**
   * Sets the Zabbixhost ID.
   *
   * @param int $id
   *   The Zabbixhost id.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixhostInterface
   *   The called Zabbixhost entity.
   */
  public function setHostid($id);

    /**
     * Gets the Zabbixhost id.
     *
     * @return integer
     *   ID of the Zabbixhost.
     */
    public function getHostid();

    /**
     * Sets the Zabbixhost name.
     *
     * @param string $name
     *   The Zabbixhost name.
     *
     * @return \Drupal\zabbixentities\Entity\zabbixhostInterface
     *   The called Zabbixhost entity.
     */
    public function setName($name);

  /**
   * Gets the Zabbixhost creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Zabbixhost.
   */
  public function getCreatedTime();

    /**
     * Sets the Zabbixhost dashboard.
     *
     * @param string $dashboard
     *   The Zabbixhost id.
     *
     * @return \Drupal\zabbixentities\Entity\zabbixhostInterface
     *   The called Zabbixhost entity.
     */
    public function setDashboard($dashboard);

    /**
     * Gets the Zabbixhost dashboard link.
     *
     * @return string
     *   Dashboard of the Zabbixhost.
     */
    public function getDashboard();


  /**
   * Sets the Zabbixhost creation timestamp.
   *
   * @param int $timestamp
   *   The Zabbixhost creation timestamp.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixhostInterface
   *   The called Zabbixhost entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Zabbixhost published status indicator.
   *
   * Unpublished Zabbixhost are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Zabbixhost is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Zabbixhost.
   *
   * @param bool $published
   *   TRUE to set this Zabbixhost to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixhostInterface
   *   The called Zabbixhost entity.
   */
  public function setPublished($published);

  /**
   * Gets the Zabbixhost revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Zabbixhost revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixhostInterface
   *   The called Zabbixhost entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Zabbixhost revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Zabbixhost revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixhostInterface
   *   The called Zabbixhost entity.
   */
  public function setRevisionUserId($uid);

}
