<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Zabbixtemplate entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixtemplateInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Zabbixtemplate name.
   *
   * @return string
   *   Name of the Zabbixtemplate.
   */
  public function getName();

  /**
   * Sets the Zabbixtemplate name.
   *
   * @param string $name
   *   The Zabbixtemplate name.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtemplateInterface
   *   The called Zabbixtemplate entity.
   */
  public function setName($name);

  /**
   * Gets the Zabbixtemplate creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Zabbixtemplate.
   */
  public function getCreatedTime();

  /**
   * Sets the Zabbixtemplate creation timestamp.
   *
   * @param int $timestamp
   *   The Zabbixtemplate creation timestamp.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtemplateInterface
   *   The called Zabbixtemplate entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Zabbixtemplate published status indicator.
   *
   * Unpublished Zabbixtemplate are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Zabbixtemplate is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Zabbixtemplate.
   *
   * @param bool $published
   *   TRUE to set this Zabbixtemplate to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtemplateInterface
   *   The called Zabbixtemplate entity.
   */
  public function setPublished($published);

  /**
   * Gets the Zabbixtemplate revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Zabbixtemplate revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtemplateInterface
   *   The called Zabbixtemplate entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Zabbixtemplate revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Zabbixtemplate revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtemplateInterface
   *   The called Zabbixtemplate entity.
   */
  public function setRevisionUserId($uid);

    /**
     * Gets the Zabbixtemplate id
     *
     * @return \Drupal\zabbixentities\Entity\zabbixtemplateInterface
     *   The called Zabbixtemplate entity.
     */
   public function getTemplateid();


    /**
     * Sets the Zabbixtemplate template ID.
     *
     * @param int $template_id
     *   The template ID.
     *
     * @return \Drupal\zabbixentities\Entity\zabbixtemplateInterface
     *   The called Zabbixtemplate entity.
     */

    public function setTemplateid($template_id);
}
