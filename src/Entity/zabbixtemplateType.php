<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Zabbixtemplate type entity.
 *
 * @ConfigEntityType(
 *   id = "zabbixtemplate_type",
 *   label = @Translation("Zabbixtemplate type"),
 *   handlers = {
 *     "list_builder" = "Drupal\zabbixentities\zabbixtemplateTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zabbixentities\Form\zabbixtemplateTypeForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixtemplateTypeForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixtemplateTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixtemplateTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zabbixtemplate_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zabbixtemplate",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/zabbixtemplate_type/{zabbixtemplate_type}",
 *     "add-form" = "/admin/structure/zabbixtemplate_type/add",
 *     "edit-form" = "/admin/structure/zabbixtemplate_type/{zabbixtemplate_type}/edit",
 *     "delete-form" = "/admin/structure/zabbixtemplate_type/{zabbixtemplate_type}/delete",
 *     "collection" = "/admin/structure/zabbixtemplate_type"
 *   }
 * )
 */
class zabbixtemplateType extends ConfigEntityBundleBase implements zabbixtemplateTypeInterface {

  /**
   * The Zabbixtemplate type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Zabbixtemplate type label.
   *
   * @var string
   */
  protected $label;

}
