<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Zabbixhost type entity.
 *
 * @ConfigEntityType(
 *   id = "zabbixhost_type",
 *   label = @Translation("Zabbixhost type"),
 *   handlers = {
 *     "list_builder" = "Drupal\zabbixentities\zabbixhostTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zabbixentities\Form\zabbixhostTypeForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixhostTypeForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixhostTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixhostTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zabbixhost_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zabbixhost",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/zabbixhost_type/{zabbixhost_type}",
 *     "add-form" = "/admin/structure/zabbixhost_type/add",
 *     "edit-form" = "/admin/structure/zabbixhost_type/{zabbixhost_type}/edit",
 *     "delete-form" = "/admin/structure/zabbixhost_type/{zabbixhost_type}/delete",
 *     "collection" = "/admin/structure/zabbixhost_type"
 *   }
 * )
 */
class zabbixhostType extends ConfigEntityBundleBase implements zabbixhostTypeInterface {

  /**
   * The Zabbixhost type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Zabbixhost type label.
   *
   * @var string
   */
  protected $label;

}
