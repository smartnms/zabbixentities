<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\zabbixentities\Plugin\Field\FieldType\zabbixinterfaceFieldType;
use Drupal\zabbixfrontend\zabbix_api;
use \Drupal\Core\Link;


/**
 * Defines the Zabbixhost entity.
 *
 * @ingroup zabbixentities
 *
 * @ContentEntityType(
 *   id = "zabbixhost",
 *   label = @Translation("Zabbixhost"),
 *   bundle_label = @Translation("Zabbixhost type"),
 *   handlers = {
 *     "storage" = "Drupal\zabbixentities\zabbixhostStorage",
 *     "view_builder" = "Drupal\zabbixentities\zabbixhostViewBuilder",
 *     "list_builder" = "Drupal\zabbixentities\zabbixhostListBuilder",
 *     "views_data" = "Drupal\zabbixentities\Entity\zabbixhostViewsData",
 *     "translation" = "Drupal\zabbixentities\zabbixhostTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\zabbixentities\Form\zabbixhostForm",
 *       "add" = "Drupal\zabbixentities\Form\zabbixhostForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixhostForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixhostDeleteForm",
 *     },
 *     "access" = "Drupal\zabbixentities\zabbixhostAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixhostHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "zabbixhost",
 *   data_table = "zabbixhost_field_data",
 *   revision_table = "zabbixhost_revision",
 *   revision_data_table = "zabbixhost_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer zabbixhost entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/smart/host/zabbixhost/{zabbixhost}",
 *     "add-page" = "/smart/host/zabbixhost/add",
 *     "add-form" = "/smart/host/zabbixhost/add/{zabbixhost_type}",
 *     "edit-form" = "/smart/host/zabbixhost/{zabbixhost}/edit",
 *     "delete-form" = "/smart/host/zabbixhost/{zabbixhost}/delete",
 *     "version-history" = "/smart/host/zabbixhost/{zabbixhost}/revisions",
 *     "revision" = "/smart/host/zabbixhost/{zabbixhost}/revisions/{zabbixhost_revision}/view",
 *     "revision_revert" = "/smart/host/zabbixhost/{zabbixhost}/revisions/{zabbixhost_revision}/revert",
 *     "translation_revert" = "/smart/host/zabbixhost/{zabbixhost}/revisions/{zabbixhost_revision}/revert/{langcode}",
 *     "revision_delete" = "/smart/host/zabbixhost/{zabbixhost}/revisions/{zabbixhost_revision}/delete",
 *     "collection" = "/smart/host/zabbixhost",
 *   },
 *   bundle_entity_type = "zabbixhost_type",
 *   field_ui_base_route = "entity.zabbixhost_type.edit_form"
 * )
 */
class zabbixhost extends RevisionableContentEntityBase implements zabbixhostInterface
{

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
    {
        parent::preCreate($storage_controller, $values);
        $values += [
            'user_id' => \Drupal::currentUser()->id(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function preSave(EntityStorageInterface $storage)
    {
        parent::preSave($storage);

        foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
            $translation = $this->getTranslation($langcode);

            // If no owner has been set explicitly, make the anonymous user the owner.
            if (!$translation->getOwner()) {
                $translation->setOwnerId(0);
            }
        }

        // If no revision author has been set explicitly, make the zabbixhost owner the
        // revision author.
        if (!$this->getRevisionUser()) {
            $this->setRevisionUserId($this->getOwnerId());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getInterface()
    {
        return $this->get('interface')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setInterface($interface)
    {
        $this->set('interface', $interface);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime()
    {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp)
    {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner()
    {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId()
    {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid)
    {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account)
    {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished()
    {
        return (bool)$this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published)
    {
        $this->set('status', $published ? TRUE : FALSE);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDashboard()
    {
        return $this->get('dashboard')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setDashboard($dashboard)
    {
        $this->set('dashboard', $dashboard);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHostid()
    {
        return $this->get('host_id')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setHostid($id)
    {
        $this->set('host_id', $id);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGlpiid()
    {
        return $this->get('glpi_id')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setGlpiid($id)
    {
        $this->set('glpi_id', $id);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Authored by'))
            ->setDescription(t('The user ID of author of the Zabbixhost entity.'))
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'user')
            ->setSetting('handler', 'default')
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'label' => 'hidden',
                'type' => 'author',
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'autocomplete_type' => 'tags',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['name'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Name'))
            ->setDescription(t('The name of the Zabbixhost entity.'))
            ->setRevisionable(TRUE)
            ->setSettings([
                'max_length' => 50,
                'text_processing' => 0,
            ])
            ->setDefaultValue('')
            ->setDisplayOptions('view', [
                'label' => 'above',
                'type' => 'string',
                'weight' => -4,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
                'weight' => -4,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        $fields['group_id'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Group'))
            ->setDescription(t('ID of group this Zabbix host should be added.'))
            ->setRevisionable(TRUE)
            ->setSetting('target_type', 'zabbixgroup')
            ->setSetting('handler', 'default')
            ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'entity_reference_autocomplete',
                'weight' => 5,
                'settings' => [
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'placeholder' => '',
                ],
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);
        $fields['host_id'] = BaseFieldDefinition::create('integer')
            ->setLabel(t('Host ID in zabbix'))
            ->setDescription(t('The host ID in Zabbix.'))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['glpi_id'] = BaseFieldDefinition::create('integer')
            ->setLabel(t('Host ID in GLPI'))
            ->setDescription(t('The host ID in GLPI.'))
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);


        $fields['interface'] = BaseFieldDefinition::create('zabbixinterface_field_type')
            ->setLabel(t('Interface'))
            ->setDescription(t('Interface details for Zabbix host.'))
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'type' => 'zabbixinterface_field_type',
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'zabbixinterface_field_type',
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['dashboard'] = BaseFieldDefinition::create('uri')
            ->setLabel(t('Dashboard URL'))
            ->setDescription(t('URL to server specific dashboard.'))
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE)
            ->setDisplayOptions('view', [
                'weight' => 0,
            ])
            ->setDisplayOptions('form', [
                'type' => 'string_textfield',
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['status'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Publishing status'))
            ->setDescription(t('A boolean indicating whether the Zabbixhost is published.'))
            ->setRevisionable(TRUE)
            ->setDefaultValue(TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Changed'))
            ->setDescription(t('The time that the entity was last edited.'));

        $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
            ->setLabel(t('Revision translation affected'))
            ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
            ->setReadOnly(TRUE)
            ->setRevisionable(TRUE)
            ->setTranslatable(TRUE);

        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function serverStatus()
    {
        $api = new zabbix_api();
        $resultp = $api->hostGet(array('hostids' => array($this->getHostid()),
            'selectTriggers' => array('triggerid', 'description', 'priority', 'value', 'url'),
            'output' => array('hostid', 'name', 'available', 'error', 'ipmi_available', 'ipmi_error',
                'jmx_available', 'jmx_error', 'snmp_available', 'snmp_error')));
        foreach ($resultp as $result) {
            $output = array();
            $output['zbx']['value'] = (int)$result['available'];
            if ($output['zbx']['value'] == 2)
                $output['zbx']['error'] = (string)$result['error'];
            $output['snmp']['value'] = (int)$result['snmp_available'];
            if ($output['snmp']['value'] == 2)
                $output['snmp']['error'] = (string)$result['snmp_error'];
            $output['ipmi']['value'] = (int)$result['ipmi_available'];
            if ($output['ipmi']['value'] == 2)
                $output['ipmi']['error'] = (string)$result['ipmi_error'];
            $output['jmx']['value'] = (int)$result['jmx_available'];
            if ($output['jmx']['value'] == 2)
                $output['jmx']['error'] = (string)$result['jmx_error'];
            foreach ($result['triggers'] as $trigger) {
                if ($trigger['value'] == 1) {
                    $output['triggers'][] = $trigger;
                }
            }
        }

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function renderStatus()
    {
        $status=$this->serverStatus();
        $output=array();
        // ZBX Agent
        $output['zbxstatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'ZBX');
        if ($status['zbx']['value'] == 1)
            $output['zbxstatus']['#attributes']=array('class'=>'statusok');
        elseif ($status['zbx']['value'] == 2) {
            $output['zbxstatus']['#attributes'] = array('class' => 'statuserror','title'=> $status['zbx']['error']);
        }
        else
            $output['zbxstatus']['#attributes']=array('class'=>'statusunknown');
        // SNMP
        if ($status['snmp']['value'] == 1) {
            $output['snmpstatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'SNMP');
            $output['snmpstatus']['#attributes']=array('class'=>'statusok');
        }
        elseif ($status['snmp']['value'] == 2) {
            $output['snmpstatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'SNMP');
            $output['snmpstatus']['#attributes']=array('class' => 'statuserror','title'=> $status['snmp']['error']);
        }
        // IPMI
        if ($status['ipmi']['value'] == 1) {
            $output['ipmistatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'IPMI');
            $output['ipmistatus']['#attributes']=array('class'=>'statusok');
        }
        elseif ($status['ipmi']['value'] == 2) {
            $output['ipmistatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'IPMI');
            $output['ipmistatus']['#attributes']=array('class' => 'statuserror','title'=> $status['ipmi']['error']);
        }

        // JMX
        if ($status['jmx']['value'] == 1) {
            $output['jmxstatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'JMX');
            $output['jmxstatus']['#attributes']=array('class'=>'statusok');
        }
        elseif ($status['jmx']['value'] == 2) {
            $output['jmxstatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'JMX');
            $output['jmxstatus']['#attributes']=array('class' => 'statuserror','title'=> $status['jmx']['error']);
        }
        $max_priority=0;
        $errortext='';
        foreach($status['triggers'] as $trigger) {
            if($trigger['priority']>$max_priority)
                $max_priority=$trigger['priority'];
            $errortext .= str_replace('{HOST.NAME}',$this->getName(),$trigger['description'] ). "\n";
        }
        $output['triggerstatus']= array('#type'=>'html_tag','#tag'=>'span','#value'=>'TRIGGER');
        $output['triggerstatus']['#attributes']=array('class' => 'trigger_'.$max_priority,'title'=> $errortext);
        return $output;
    }

    public function renderTriggers()
    {
        $api = new zabbix_api();
        $resultp = $api->hostGet(array('hostids' => array($this->getHostid()),
            'selectTriggers' => array('triggerid', 'description', 'priority', 'value'),
            'output' => array('hostid')));
        foreach ($resultp as $result) {
            $output = array();
            $header=array('trigger'=>'Trigger');
            $rows=array();
            foreach ($result['triggers'] as $trigger) {
                if ($trigger['value'] == 1) {
                    $errortext = str_replace('{HOST.NAME}',$this->getName(),$trigger['description'] );
                    $query = \Drupal::entityQuery('zabbixtrigger');
                    $query->condition('trigger_id', $trigger['triggerid']);
                    $entity_ids = $query->execute();
                    $ids=array_keys($entity_ids);
                    if(is_array($ids))
                        $trigger_entity=\Drupal::entityTypeManager()->getStorage('zabbixtrigger')->load($ids[0]);
                    else
                        $trigger_entity=\Drupal::entityTypeManager()->getStorage('zabbixtrigger')->load($ids);
                   if(!is_null($trigger_entity)) {
                       $url = $trigger_entity->toUrl('canonical', array());
                       $link = Link::fromTextAndUrl($errortext, $url);
                       $rows[] = ['trigger' => array('data' => array('#type' => 'html_tag', '#tag' => 'span', '#value' => $link->toString() . "<br />",
                           '#attributes' => array('class' => 'trigger_' . $trigger['priority'], 'title' => $errortext)))];
                   }
                }
            }
        }
        if(count($rows)==0)
            $rows=array('trigger'=>'No hay triggers definidos');
        $output['tabla']=array('#type'=>'table',
                                '#header'=>$header,
                                '#rows'=>$rows);
        return $output;
    }
}