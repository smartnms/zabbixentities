<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Zabbix Group entity.
 *
 * @ingroup zabbixentities
 *
 * @ContentEntityType(
 *   id = "zabbixgroup",
 *   label = @Translation("Zabbix Group"),
 *   bundle_label = @Translation("Zabbix Group type"),
 *   handlers = {
 *     "storage" = "Drupal\zabbixentities\zabbixgroupStorage",
 *     "view_builder" = "Drupal\zabbixentities\zabbixgroupViewBuilder",
 *     "list_builder" = "Drupal\zabbixentities\zabbixgroupListBuilder",
 *     "views_data" = "Drupal\zabbixentities\Entity\zabbixgroupViewsData",
 *     "translation" = "Drupal\zabbixentities\zabbixgroupTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\zabbixentities\Form\zabbixgroupForm",
 *       "add" = "Drupal\zabbixentities\Form\zabbixgroupForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixgroupForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixgroupDeleteForm",
 *     },
 *     "access" = "Drupal\zabbixentities\zabbixgroupAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixgroupHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "zabbixgroup",
 *   data_table = "zabbixgroup_field_data",
 *   revision_table = "zabbixgroup_revision",
 *   revision_data_table = "zabbixgroup_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer group as defined in zabbix entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/smart/structure/zabbixgroup/{zabbixgroup}",
 *     "add-page" = "/smart/structure/zabbixgroup/add",
 *     "add-form" = "/smart/structure/zabbixgroup/add/{zabbixgroup_type}",
 *     "edit-form" = "/smart/structure/zabbixgroup/{zabbixgroup}/edit",
 *     "delete-form" = "/smart/structure/zabbixgroup/{zabbixgroup}/delete",
 *     "version-history" = "/smart/structure/zabbixgroup/{zabbixgroup}/revisions",
 *     "revision" = "/smart/structure/zabbixgroup/{zabbixgroup}/revisions/{zabbixgroup_revision}/view",
 *     "revision_revert" = "/smart/structure/zabbixgroup/{zabbixgroup}/revisions/{zabbixgroup_revision}/revert",
 *     "translation_revert" = "/smart/structure/zabbixgroup/{zabbixgroup}/revisions/{zabbixgroup_revision}/revert/{langcode}",
 *     "revision_delete" = "/smart/structure/zabbixgroup/{zabbixgroup}/revisions/{zabbixgroup_revision}/delete",
 *     "collection" = "/smart/structure/zabbixgroup",
 *   },
 *   bundle_entity_type = "zabbixgroup_type",
 *   field_ui_base_route = "entity.zabbixgroup_type.edit_form"
 * )
 */
class zabbixgroup extends RevisionableContentEntityBase implements zabbixgroupInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the zabbixgroup owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public function setGroupid($groupid) {
    $this->set('group_id', $groupid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupid() {
    return $this->get('group_id')->value;
  }

    /**
     * {@inheritdoc}
     */
    public function setGrouptype($grouptype) {
        $this->set('group_type', $grouptype);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getGrouptype() {
        return $this->get('group_type')->value;
    }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setDescription(t('The user ID of creator of the Group.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'creator',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

	$fields['group_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Group Id in zabbix'))
      ->setDescription(t('The group ID in Zabbix.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Group in Zabbix.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active status'))
      ->setDescription(t('A boolean indicating whether the Group in Zabbix is active.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the group entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the group entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

      $fields['group_type'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Group Type'))
          ->setDescription(t('Indicates the type of group.'))
          ->setRevisionable(TRUE)
          ->setTranslatable(TRUE);
    return $fields;
  }

}
