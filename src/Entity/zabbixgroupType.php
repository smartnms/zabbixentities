<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Group as defined in Zabbix type entity.
 *
 * @ConfigEntityType(
 *   id = "zabbixgroup_type",
 *   label = @Translation("Group as defined in Zabbix type"),
 *   handlers = {
 *     "list_builder" = "Drupal\zabbixentities\zabbixgroupTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zabbixentities\Form\zabbixgroupTypeForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixgroupTypeForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixgroupTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixgroupTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zabbixgroup_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zabbixgroup",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/zabbixgroup_type/{zabbixgroup_type}",
 *     "add-form" = "/admin/structure/zabbixgroup_type/add",
 *     "edit-form" = "/admin/structure/zabbixgroup_type/{zabbixgroup_type}/edit",
 *     "delete-form" = "/admin/structure/zabbixgroup_type/{zabbixgroup_type}/delete",
 *     "collection" = "/admin/structure/zabbixgroup_type"
 *   }
 * )
 */
class zabbixgroupType extends ConfigEntityBundleBase implements zabbixgroupTypeInterface {

  /**
   * The Group as defined in Zabbix type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Group as defined in Zabbix type label.
   *
   * @var string
   */
  protected $label;

}
