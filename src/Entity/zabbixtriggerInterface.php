<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Zabbixtrigger entities.
 *
 * @ingroup zabbixentities
 */
interface zabbixtriggerInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Zabbixtrigger name.
   *
   * @return string
   *   Name of the Zabbixtrigger.
   */
  public function getName();

  /**
   * Sets the Zabbixtrigger name.
   *
   * @param string $name
   *   The Zabbixtrigger name.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtriggerInterface
   *   The called Zabbixtrigger entity.
   */
  public function setName($name);

  /**
   * Gets the Zabbixtrigger creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Zabbixtrigger.
   */
  public function getCreatedTime();

  /**
   * Sets the Zabbixtrigger creation timestamp.
   *
   * @param int $timestamp
   *   The Zabbixtrigger creation timestamp.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtriggerInterface
   *   The called Zabbixtrigger entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Zabbixtrigger published status indicator.
   *
   * Unpublished Zabbixtrigger are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Zabbixtrigger is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Zabbixtrigger.
   *
   * @param bool $published
   *   TRUE to set this Zabbixtrigger to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtriggerInterface
   *   The called Zabbixtrigger entity.
   */
  public function setPublished($published);

  /**
   * Gets the Zabbixtrigger revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Zabbixtrigger revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtriggerInterface
   *   The called Zabbixtrigger entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Zabbixtrigger revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Zabbixtrigger revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\zabbixentities\Entity\zabbixtriggerInterface
   *   The called Zabbixtrigger entity.
   */
  public function setRevisionUserId($uid);


    /**
     * Sets the Zabbixtrigger ID.
     *
     * @param int $id
     *   The ID of the trigger.
     *
     * @return \Drupal\zabbixentities\Entity\zabbixtriggerInterface
     *   The called Zabbixtrigger entity.
     */
    public function setTriggerd($id);

    /**
     * Sets the Zabbixtrigger ID.
     *
     * @param
     *   The ID of the trigger.
     *
     * @return int $id
     *   The ID of the trigger.
     */
    public function getTriggerid();



}
