<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Zabbixtrigger entity.
 *
 * @ingroup zabbixentities
 *
 * @ContentEntityType(
 *   id = "zabbixtrigger",
 *   label = @Translation("Trigger Zabbix"),
 *   bundle_label = @Translation("Trigger Type"),
 *   handlers = {
 *     "storage" = "Drupal\zabbixentities\zabbixtriggerStorage",
 *     "view_builder" = "Drupal\zabbixentities\zabbixtriggerViewBuilder",
 *     "list_builder" = "Drupal\zabbixentities\zabbixtriggerListBuilder",
 *     "views_data" = "Drupal\zabbixentities\Entity\zabbixtriggerViewsData",
 *     "translation" = "Drupal\zabbixentities\zabbixtriggerTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\zabbixentities\Form\zabbixtriggerForm",
 *       "add" = "Drupal\zabbixentities\Form\zabbixtriggerForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixtriggerForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixtriggerDeleteForm",
 *     },
 *     "access" = "Drupal\zabbixentities\zabbixtriggerAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixtriggerHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "zabbixtrigger",
 *   data_table = "zabbixtrigger_field_data",
 *   revision_table = "zabbixtrigger_revision",
 *   revision_data_table = "zabbixtrigger_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer zabbixtrigger entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/smart/structure/zabbixtrigger/{zabbixtrigger}",
 *     "add-page" = "/smart/structure/zabbixtrigger/add",
 *     "add-form" = "/smart/structure/zabbixtrigger/add/{zabbixtrigger_type}",
 *     "edit-form" = "/smart/structure/zabbixtrigger/{zabbixtrigger}/edit",
 *     "delete-form" = "/smart/structure/zabbixtrigger/{zabbixtrigger}/delete",
 *     "version-history" = "/smart/structure/zabbixtrigger/{zabbixtrigger}/revisions",
 *     "revision" = "/smart/structure/zabbixtrigger/{zabbixtrigger}/revisions/{zabbixtrigger_revision}/view",
 *     "revision_revert" = "/smart/structure/zabbixtrigger/{zabbixtrigger}/revisions/{zabbixtrigger_revision}/revert",
 *     "translation_revert" = "/smart/structure/zabbixtrigger/{zabbixtrigger}/revisions/{zabbixtrigger_revision}/revert/{langcode}",
 *     "revision_delete" = "/smart/structure/zabbixtrigger/{zabbixtrigger}/revisions/{zabbixtrigger_revision}/delete",
 *     "collection" = "/smart/structure/zabbixtrigger",
 *   },
 *   bundle_entity_type = "zabbixtrigger_type",
 *   field_ui_base_route = "entity.zabbixtrigger_type.edit_form"
 * )
 */
class zabbixtrigger extends RevisionableContentEntityBase implements zabbixtriggerInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the zabbixtrigger owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

    /**
     * {@inheritdoc}
     */
    public function getTriggerid() {
        return $this->get('trigger_id')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setTriggerid($id) {
        $this->set('trigger_id', $id);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getServer() {
        return $this->get('server')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setServer($server) {
        $this->set('server', $server);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getKey() {
        return $this->get('key')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setKey($key) {
        $this->set('key', $key);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getFunction() {
        return $this->get('function')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setFunction($function) {
        $this->set('function', $function);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getParameter() {
        return $this->get('parameter')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setParameter($parameter) {
        $this->set('parameter', $parameter);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getOperator() {
        return $this->get('operator')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setOperator($operator) {
        $this->set('operator', $operator);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getConstant() {
        return $this->get('constant')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setConstant($constant) {
        $this->set('constant', $constant);
        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function getComments() {
        return $this->get('comments')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setComments($comments) {
        $this->set('comments', $comments);
        return $this;
    }


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Zabbixtrigger entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['trigger_id'] = BaseFieldDefinition::create('integer')
          ->setLabel(t('Trigger Id in zabbix'))
          ->setDescription(t('The ID of the trigger as defined in Zabbix entity.'));

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The name of the Zabbixtrigger entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 256,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['server'] = BaseFieldDefinition::create('entity_reference')
          ->setLabel(t('Server'))
          ->setDescription(t('The Server name this trigger applies to.'))
          ->setRevisionable(TRUE)
          ->setSetting('target_type', 'user')
          ->setSetting('handler', 'default')
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'entity_reference_autocomplete',
              'weight' => 5,
              'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => '60',
                  'autocomplete_type' => 'tags',
                  'placeholder' => '',
              ],
          ])
          ->setDisplayConfigurable('form', FALSE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['key'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Key'))
          ->setDescription(t('The expression of the key evaluated by trigger.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 256,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 0,
          ])
          ->setDisplayConfigurable('form', FALSE)
          ->setDisplayConfigurable('view', TRUE);
      $fields['function'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Function'))
          ->setDescription(t('The Function to evaluate.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 256,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 0,
          ])
          ->setDisplayConfigurable('form', FALSE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['parameter'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Parameter'))
          ->setDescription(t('The parameters of the function.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 256,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 0,
          ])
          ->setDisplayConfigurable('form', FALSE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['operator'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Operator'))
          ->setDescription(t('The operator to evaluate.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 256,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 0,
          ])
          ->setDisplayConfigurable('form', FALSE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['constant'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Constant'))
          ->setDescription(t('The constant to evaluate trigger against.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 256,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 0,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['severity'] = BaseFieldDefinition::create('list_string')
          ->setLabel(t('Severity'))
          ->setDescription(t('The severity of the trigger.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 256,
              'text_processing' => 0,
              'allowed_values' =>   ['0'=> t('Not Classified'),
                  '1' => t('information'),
                  '2' => t('warning'),
                  '3' => t('average'),
                  '4' => t('high'),
                  '5' => t('disaster'),
                  ],
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'options_select',
              'weight' => 0,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['comments'] = BaseFieldDefinition::create('string')
          ->setLabel(t('comments'))
          ->setDescription(t('The comments of the Zabbixtrigger entity.'))
          ->setRevisionable(TRUE)
          ->setSettings([
              'max_length' => 256,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => -5,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textarea',
              'weight' => -5,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Zabbixtrigger is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

    /**
     * Sets the Zabbixtrigger ID.
     *
     * @param int $id
     *   The ID of the trigger.
     *
     * @return \Drupal\zabbixentities\Entity\zabbixtriggerInterface
     *   The called Zabbixtrigger entity.
     */
    public function setTriggerd($id)
    {
        $this->set('trigger_id', $id);
        return $this;
    }

}
