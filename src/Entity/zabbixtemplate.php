<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Zabbixtemplate entity.
 *
 * @ingroup zabbixentities
 *
 * @ContentEntityType(
 *   id = "zabbixtemplate",
 *   label = @Translation("Zabbixtemplate"),
 *   bundle_label = @Translation("Zabbixtemplate type"),
 *   handlers = {
 *     "storage" = "Drupal\zabbixentities\zabbixtemplateStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\zabbixentities\zabbixtemplateListBuilder",
 *     "views_data" = "Drupal\zabbixentities\Entity\zabbixtemplateViewsData",
 *     "translation" = "Drupal\zabbixentities\zabbixtemplateTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\zabbixentities\Form\zabbixtemplateForm",
 *       "add" = "Drupal\zabbixentities\Form\zabbixtemplateForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixtemplateForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixtemplateDeleteForm",
 *     },
 *     "access" = "Drupal\zabbixentities\zabbixtemplateAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixtemplateHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "zabbixtemplate",
 *   data_table = "zabbixtemplate_field_data",
 *   revision_table = "zabbixtemplate_revision",
 *   revision_data_table = "zabbixtemplate_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer zabbixtemplate entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/smart/template/zabbixtemplate/{zabbixtemplate}",
 *     "add-page" = "/smart/template/zabbixtemplate/add",
 *     "add-form" = "/smart/template/zabbixtemplate/add/{zabbixtemplate_type}",
 *     "edit-form" = "/smart/template/zabbixtemplate/{zabbixtemplate}/edit",
 *     "delete-form" = "/smart/template/zabbixtemplate/{zabbixtemplate}/delete",
 *     "version-history" = "/smart/template/zabbixtemplate/{zabbixtemplate}/revisions",
 *     "revision" = "/smart/template/zabbixtemplate/{zabbixtemplate}/revisions/{zabbixtemplate_revision}/view",
 *     "revision_revert" = "/smart/template/zabbixtemplate/{zabbixtemplate}/revisions/{zabbixtemplate_revision}/revert",
 *     "translation_revert" = "/smart/template/zabbixtemplate/{zabbixtemplate}/revisions/{zabbixtemplate_revision}/revert/{langcode}",
 *     "revision_delete" = "/smart/template/zabbixtemplate/{zabbixtemplate}/revisions/{zabbixtemplate_revision}/delete",
 *     "collection" = "/smart/template/zabbixtemplate",
 *   },
 *   bundle_entity_type = "zabbixtemplate_type",
 *   field_ui_base_route = "entity.zabbixtemplate_type.edit_form"
 * )
 */
class zabbixtemplate extends RevisionableContentEntityBase implements zabbixtemplateInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the zabbixtemplate owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->entity;
  }
  
  /**
   * {@inheritdoc}
   */
   
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }
  /**
   * {@inheritdoc}
   */
  public function getTemplateid() {
    return $this->get('template_id')->value;
  }
  
  /**
   * {@inheritdoc}
   */
   
  public function setTemplateid($id) {
    $this->set('template_id', $id);
    return $this;
  }

    /**
     * {@inheritdoc}
     */
    public function getGroupid() {
        return $this->get('group_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setGroupid($group_id) {
        $this->set('group_id', $group_id);
        return $this;
    }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Zabbixtemplate entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
	$fields['template_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Template Id in zabbix'))
      ->setDescription(t('The ID of the Template as defined in Zabbix entity.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Zabbixtemplate entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
	  
    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The description of the Zabbixtemplate entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['group_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Group'))
      ->setDescription(t('ID of group affected by Zabbix template entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'zabbixgroup')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
      $fields['host_id'] = BaseFieldDefinition::create('entity_reference')
          ->setLabel(t('Host'))
          ->setDescription(t('ID of host affected by Zabbix template entity.'))
          ->setRevisionable(TRUE)
          ->setSetting('target_type', 'zabbixhost')
          ->setSetting('handler', 'default')
          ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
          ->setTranslatable(TRUE)
          ->setDisplayOptions('view', [
              'weight' => 0,
          ])
          ->setDisplayOptions('form', [
              'type' => 'entity_reference_autocomplete',
              'weight' => 5,
              'settings' => [
                  'match_operator' => 'CONTAINS',
                  'size' => '60',
                  'placeholder' => '',
              ],
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);
	  $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Zabbixtemplate is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
