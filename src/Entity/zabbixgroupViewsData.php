<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Group as defined in Zabbix entities.
 */
class zabbixgroupViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
