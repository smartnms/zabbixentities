<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Zabbixhost type entities.
 */
interface zabbixhostTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
