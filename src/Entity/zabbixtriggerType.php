<?php

namespace Drupal\zabbixentities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Zabbixtrigger type entity.
 *
 * @ConfigEntityType(
 *   id = "zabbixtrigger_type",
 *   label = @Translation("Trigger type"),
 *   handlers = {
 *     "list_builder" = "Drupal\zabbixentities\zabbixtriggerTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zabbixentities\Form\zabbixtriggerTypeForm",
 *       "edit" = "Drupal\zabbixentities\Form\zabbixtriggerTypeForm",
 *       "delete" = "Drupal\zabbixentities\Form\zabbixtriggerTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zabbixentities\zabbixtriggerTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zabbixtrigger_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zabbixtrigger",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/smart/trigger/zabbixtrigger/zabbixtrigger_type/{zabbixtrigger_type}",
 *     "add-form" = "/smart/trigger/zabbixtrigger/zabbixtrigger_type/add",
 *     "edit-form" = "/smart/trigger/zabbixtrigger/zabbixtrigger_type/{zabbixtrigger_type}/edit",
 *     "delete-form" = "/smart/trigger/zabbixtrigger/zabbixtrigger_type/{zabbixtrigger_type}/delete",
 *     "collection" = "/smart/trigger/zabbixtrigger/zabbixtrigger_type"
 *   }
 * )
 */
class zabbixtriggerType extends ConfigEntityBundleBase implements zabbixtriggerTypeInterface {

  /**
   * The Zabbixtrigger type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Zabbixtrigger type label.
   *
   * @var string
   */
  protected $label;

}
