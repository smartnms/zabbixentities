<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Zabbixtemplate entities.
 *
 * @ingroup zabbixentities
 */
class zabbixtemplateListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Template ID');
    $header['template_id'] = $this->t('Template ID in Zabbix');
    $header['name'] = $this->t('Name');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\zabbixentities\Entity\zabbixtemplate */
    $row['id'] = $entity->id();
    $row['template_id']=$entity->getTemplateid();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.zabbixtemplate.canonical', [
          'zabbixtemplate' => $entity->id(),
        ]
      )
    );
    return $row + parent::buildRow($entity);
  }

}
