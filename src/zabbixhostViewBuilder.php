<?php

namespace Drupal\zabbixentities;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\zabbixentities\Entity\zabbixhost;
use Drupal\glpiinventory\glpi_api;
use Drupal\Core\Url;


/**
 * Render controller for zabbixhost.
 */

class zabbixhostViewBuilder extends EntityViewBuilder {


    public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
        $glpi_tags=array('id' => 'ID:', 'entities_id' => t('Entity ID:'),
            'name' => t('Name:'), 'serial' => t('Serial Number:'),
            'otherserial' => t('Additional Serial Number:'), 'status_id'=> t('Status'),'contact' => t('Contact:'),
            'contact_num' => t('Contact Number:'), 'users_id_tech' => t('ID of technical user:'),
            'groups_id_tech' => t('ID of technical Group:'), 'comment' => t('Comment'),
            'date_mod' => t('Last modified:'), 'operatingsystems_id' => t('Operating System ID'),
            'operatingsystemversions_id' => t('Operating system Version ID:'),
            'operatingsystemservicepacks_id' => t('Service Pack ID:'), 'operatingsystemarchitectures_id' => t('Architecture ID'),
            'os_license_number' => t('OS License Number:'),'os_licenseid' =>t('OS License ID:'),
            'os_kernel_version' => t('OS Kernel Version:'),
            'locations_id' => t('Locations ID:'), 'domains_id' => t('Domains ID:'),
            'networks_id' => t('Networks ID:' ),'computermodels_id' => t('Computer Model ID:'),
            'computertypes_id' => t('Computer Type ID:'),'manufacturers_id' => t('Manufacturer ID:'),
            'date_creation' => t('Creation Date:'));
        /** @var \Drupal\node\NodeInterface[] $entities */
        if (empty($entities)) {
            return;
        }

        parent::buildComponents($build, $entities, $displays, $view_mode);

                foreach ($entities as $id => $entity) {
            // We put the display components on a different detail
            $build[$id]['#attached']['library'][] = 'zabbixentities/smartnms';
            $build[$id]['fields']=array('#type'=>'details',
                '#title' => $this->t('Host Details'),
                '#description' => t('Custom values for host'),
                '#open' => FALSE,);
            foreach($build[$id] as $name => $attribute) {
                if ($entity->hasField($name)) {
                    $build[$id]['fields'][$name] = $attribute;
                    unset($build[$id][$name]);
                }
            }
            $zabbix_uri=\Drupal::l(t('Zabbix Link'), Url::fromUri("http://82.223.78.156/zabbix/latest.php?filter_set=1&hostids[]=".$entity->getHostid(),
                array('attributes' => ['target' => '_blank'])));
            $glpi_uri=\Drupal::l(t('GLPI Link'), Url::fromUri("http://82.223.80.4/glpi/front/computer.form.php?id=".$entity->getGlpiid(),
                array('attributes' => ['target' => '_blank'])));
            if(is_null($entity->getDashboard()))
                $dashboard='http://82.223.80.4/stats/dashboard/db/estado-general?orgId=1';
            else
                $dashboard=$entity->getDashboard();
            $dashboardlink=\Drupal::l('Dashboard', Url::fromUri($dashboard, array('attributes' => ['target' =>'_blank'])));
            $build[$id]['enlaces']=array('#type'=>'details',
                '#title' => $this->t('Links'),
                '#description' => t('Links of host components'),
                '#open' => TRUE,);
            $build[$id]['enlaces']['glpi']= array('#type'=>'html_tag','#tag'=>'span','#value'=>$glpi_uri);
            $build[$id]['enlaces']['glpi']['#attributes']=array('class'=>['btn','btn-primary','btn-xs']);
            $build[$id]['enlaces']['dashboard']= array('#type'=>'html_tag','#tag'=>'span','#value'=>$dashboardlink);
            $build[$id]['enlaces']['dashboard']['#attributes']=array('class'=>['btn','btn-primary','btn-xs']);
            $build[$id]['enlaces']['zabbix']= array('#type'=>'html_tag','#tag'=>'span','#value'=>$zabbix_uri);
            $build[$id]['enlaces']['zabbix']['#attributes']=array('class'=>['btn','btn-primary','btn-xs']);
            // Add Status to render array.
            $build[$id]['status']=array('#type'=>'details',
                                    '#title' => $this->t('Status'),
                                    '#description' => t('Status summary of services in the host'),
                                    '#open' => TRUE,);
            $build[$id]['status']['componentes']=array('#type'=>'item',
                '#allowed_tags'=>array('div','span'),
                '#title'=>t('Status'),
                $entity->renderStatus());
            $build[$id]['status']['triggers']=array('#type'=>'item',
                '#allowed_tags'=>array('div','span'),
                '#title'=>t('Triggers'),
                $entity->renderTriggers());

            $build[$id]['glpi']=array('#type'=>'details',
                '#title' => $this->t('GLPI details'),
                '#description' => t('Inventory details of Host stored on GLPI'),
                '#open' => FALSE,);
            $glpiid=$entity->getGlpiid();
            if(!is_null($glpiid)) {
                $api = new glpi_api();
                $loadoutput = $api->getitem('Computer', $glpiid, array('expand_dropdowns' => 'true'));
                $result = array_combine(array_intersect_key($glpi_tags, $loadoutput), array_intersect_key($loadoutput, $glpi_tags));
                $rows=array();
                foreach($result as $key => $value)
                    $rows[]=[$key,$value];
                $build[$id]['glpi']['processed']=array('#type' =>'table',
                    '#header'=>array(t('Key'),t('Value')),
                    '#rows' => $rows);
            }
            else {
                $build[$id]['glpi']['processed']=array('#type'=>'html_tag','#tag'=>'span','#value'=>t('This host is not linked to GLPI Host'));
            }
        }

    }

}

?>