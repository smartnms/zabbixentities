<?php

/**
 * @file
 * Contains zabbixtrigger.page.inc.
 *
 * Page callback for Zabbixtrigger entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Zabbixtrigger templates.
 *
 * Default template: zabbixtrigger.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zabbixtrigger(array &$variables) {
  // Fetch zabbixtrigger Entity Object.
  $zabbixtrigger = $variables['elements']['#zabbixtrigger'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
