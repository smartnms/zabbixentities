<?php

/**
 * @file
 * Contains zabbixhost.page.inc.
 *
 * Page callback for Zabbixhost entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Prepares variables for Zabbixhost templates.
 *
 * Default template: zabbixhost.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zabbixhost(array &$variables) {
  // Fetch zabbixhost Entity Object.
  $zabbixhost = $variables['elements']['#zabbixhost'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

}